using System;

namespace Models
{

    public partial class SyncronizacionDTO
    {
        public int IdSincronizacion { get; set; }
        public int CursosMigrados { get; set; }
        public int EstudiantesMigrados { get; set; }
        public int Errores { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaFin { get; set; }
        public string error { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }
        public string Usuario { get; set; }

    }

    public partial class SyncronizacionSaveDTO
    {
        public bool TipoSincronizacion { get; set; } = true;
        public int? UserId { get; set; } = null;
        public DateTime? FechaDesde { get; set; } = null;
        public DateTime Fecha { get; set; }
    }


}