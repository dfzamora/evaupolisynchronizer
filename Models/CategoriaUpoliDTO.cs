namespace moodle.Models.EnlaceEva
{
    public partial class CategoriaUpoliDTO
    {

        public int EscuelaId { get; set; }
        public string Escuela { get; set; }
        public int RecintoId { get; set; }
        public string Recinto { get; set; }
        public int CarreraId { get; set; }
        public string Carrera { get; set; }
        public string rama { get; set; }
    }
}

