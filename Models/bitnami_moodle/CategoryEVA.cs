using System.ComponentModel.DataAnnotations;

namespace moodle.Models.bitnami_moodle
{
    public partial class CategoryEVA
    {
        [Key]
        public long IdEVA { get; set; }
        public long parentEVA { get; set; }
        public string NameEVA { get; set; }
        public string RamaEVA { get; set; }
    }

}