using System.ComponentModel.DataAnnotations;

namespace moodle.Models.bitnami_moodle
{
    public partial class InscribirCursoEVA
    {
        [Key]
        public long IDCursoEva { get; set; }
        public long? category { get; set; }
        public string error { get; set; }

    }
}