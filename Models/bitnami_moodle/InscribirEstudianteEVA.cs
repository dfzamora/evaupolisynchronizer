using System.ComponentModel.DataAnnotations;

namespace moodle.Models.bitnami_moodle
{
    public partial class InscribirEstudianteEVA
    { 
        [Key]
        public long iduser{get;set;}
        public string correo{get;set;}
        public string error{get;set;}
    }
}