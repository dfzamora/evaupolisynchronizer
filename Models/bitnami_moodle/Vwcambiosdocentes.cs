﻿using System;
using System.Collections.Generic;

namespace moodle.Models.bitnami_moodle
{
    public partial class Vwcambiosdocentes
    {
        public long Id { get; set; }
        public long CursoId { get; set; }
        public string NombresAnterior { get; set; }
        public string ApellidosAnterior { get; set; }
        public string CorreoAnterior { get; set; }
        public string NombresNuevo { get; set; }
        public string ApellidosNuevo { get; set; }
        public string CorreoNuevo { get; set; }
        public string Estado { get; set; }
    }
}
