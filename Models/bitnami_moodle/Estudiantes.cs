﻿using System;
using System.Collections.Generic;

namespace moodle.Models.bitnami_moodle
{
    public partial class Estudiantes
    {
        public long Id { get; set; }
        public long? IdcursoEva { get; set; }
        public string Correo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Estado { get; set; }
    }
}
