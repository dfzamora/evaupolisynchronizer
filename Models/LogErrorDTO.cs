﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class LogErrorDTO
    {
        public LogErrorDTO(int? IdSincronizacion)
        {
            this.IdSincronizacion = IdSincronizacion;
        }



        public int IdLogError { get; set; }
        public string TipoError { get; set; }
        public string Error { get; set; }
        public int? IdSincronizacion { get; }
        public DateTime FechaError =>DateTime.Now;

    }
}
