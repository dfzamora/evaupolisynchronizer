using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace moodle.Models.EnlaceEva
{
    public partial class Usuario
    {

        [NotMapped]
        public string token { get; set; }
    }

    public partial class ServerInfo
    {
        [Key]
        public DateTime Fecha { get; set; }
    }
}
