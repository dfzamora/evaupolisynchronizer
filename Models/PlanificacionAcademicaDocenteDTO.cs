using System.ComponentModel.DataAnnotations.Schema;

namespace moodle.Models.EnlaceEva
{
    public partial class PlanificacionAcademicaDocenteDTO
    {
        public VwplanificacionAcademica planificaciones { get; set; }
        public VwperfilDocente docentes { get; set; }
    }
}
