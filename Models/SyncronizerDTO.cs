using System;

namespace Models
{

    public partial class SyncronizerDTO
    {
        public int IdSincronizacion { get; set; }
        public int cursosMigrar { get; set; }
        public int Estudiantes { get; set; }
        public int Errores { get; set; }
        public DateTime? Fecha { get; set; }
        public string error { get; set; }

    }
}
