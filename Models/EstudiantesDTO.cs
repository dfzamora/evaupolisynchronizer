namespace Models
{
    public partial class EstudiantesDTO
    {
        public string Carnet {get;set;}
        public string Nombre {get;set;}
        public string CorreoEstudiante {get;set;}

    }
}