﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwestudiantesInscritosciclosactivos
    {
        public string Carnet { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Nombre { get; set; }
        public string CorreoEstudiante { get; set; }
        public int RegionalId { get; set; }
        public string Regional { get; set; }
        public int CicloId { get; set; }
        public string Ciclo { get; set; }
        public int CursoId { get; set; }
        public string EstadoInscripcion { get; set; }
        public DateTime FechaEstado { get; set; }
        public DateTime? FechaInscripcion { get; set; }
        public DateTime? FechaRetiro { get; set; }
        public DateTime? FechaEliminado { get; set; }
        public int PerCodigo { get; set; }
    }
}
