﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwperfilDocente
    {
        public string Correo { get; set; }
        public int CodigoDocente { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string NombreCompleto { get; set; }
        public string Genero { get; set; }
    }
}
