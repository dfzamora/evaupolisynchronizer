﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwinfoEquivalente
    {
        public int IdInfoEquivalencia { get; set; }
        public int EscuelaId { get; set; }
        public string Escuela { get; set; }
        public int CarreraId { get; set; }
        public string Carrera { get; set; }
        public int RecintoId { get; set; }
        public string Recinto { get; set; }
        public long IdCategoriaEva { get; set; }
        public long IdPadreEva { get; set; }
        public string NameEva { get; set; }
        public string RamaEva { get; set; }
        public string RamaUpoli { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
