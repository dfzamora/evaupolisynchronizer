﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class InfoEquivalencia
    {
        public int IdInfoEquivalencia { get; set; }
        public int RecintoId { get; set; }
        public int EscuelaId { get; set; }
        public int CarreraId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public long IdCategoriaEva { get; set; }
        public long IdPadreEva { get; set; }
        public string NameEva { get; set; }
        public string RamaEva { get; set; }
    }
}
