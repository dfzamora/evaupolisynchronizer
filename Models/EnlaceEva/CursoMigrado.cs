﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class CursoMigrado
    {
        public int IdCursoMigrado { get; set; }
        public int CursoId { get; set; }
        public long IdcursoEva { get; set; }
        public long Categoria { get; set; }
        public int IdSincronizacion { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual Sincronizacion IdSincronizacionNavigation { get; set; }
    }
}
