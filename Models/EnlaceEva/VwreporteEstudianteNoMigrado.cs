﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwreporteEstudianteNoMigrado
    {
        public int CicloId { get; set; }
        public int CursoId { get; set; }
        public string NomeclaturaCurso { get; set; }
        public string Carnet { get; set; }
        public string Nombre { get; set; }
        public string CorreoEstudiante { get; set; }
        public DateTime? FechaInscripcion { get; set; }
        public DateTime FechaEstado { get; set; }
    }
}
