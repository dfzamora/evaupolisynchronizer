﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class EstudianteMigrado
    {
        public int IdEstudianteMigrado { get; set; }
        public int CursoId { get; set; }
        public string Carnet { get; set; }
        public int IdSincronizacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Estado { get; set; }

        public virtual Sincronizacion IdSincronizacionNavigation { get; set; }
    }
}
