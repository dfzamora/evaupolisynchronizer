﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwperfilEstudiantes
    {
        public string Correo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreCompleto { get; set; }
        public string Carnet { get; set; }
        public string Genero { get; set; }
        public int Nacionalidad { get; set; }
        public int Departamento { get; set; }
        public string Recinto { get; set; }
        public string Escuela { get; set; }
        public string Carrera { get; set; }
    }
}
