﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class Usuario
    {
        public Usuario()
        {
            Sincronizacion = new HashSet<Sincronizacion>();
        }

        public int IdUsuario { get; set; }
        public string Usuario1 { get; set; }
        public string Pass { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool IsAdmin { get; set; }

        public virtual ICollection<Sincronizacion> Sincronizacion { get; set; }
    }
}
