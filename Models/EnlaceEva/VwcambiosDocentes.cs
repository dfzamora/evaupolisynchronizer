﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwcambiosDocentes
    {
        public long CursoId { get; set; }
        public string NombresAnterior { get; set; }
        public string ApellidosAnterior { get; set; }
        public string CorreoAnterior { get; set; }
        public string NombresNuevo { get; set; }
        public string ApellidosNuevo { get; set; }
        public string CorreoNuevo { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
