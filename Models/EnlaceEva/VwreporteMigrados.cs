﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwreporteMigrados
    {
        public int CicloId { get; set; }
        public int CursoId { get; set; }
        public string NomeclaturaCurso { get; set; }
        public int? Inscritos { get; set; }
        public int? Migrados { get; set; }
    }
}
