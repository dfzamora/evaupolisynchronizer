﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class Sincronizacion
    {
        public Sincronizacion()
        {
            CursoMigrado = new HashSet<CursoMigrado>();
            EstudianteMigrado = new HashSet<EstudianteMigrado>();
            LogError = new HashSet<LogError>();
        }

        public int IdSincronizacion { get; set; }
        public int? IdUsuario { get; set; }
        public bool Tipo { get; set; }
        public bool EnProceso { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaFin { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual ICollection<CursoMigrado> CursoMigrado { get; set; }
        public virtual ICollection<EstudianteMigrado> EstudianteMigrado { get; set; }
        public virtual ICollection<LogError> LogError { get; set; }
    }
}
