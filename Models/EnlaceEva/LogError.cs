﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class LogError
    {
        public int IdLogError { get; set; }
        public string TipoError { get; set; }
        public string Error { get; set; }
        public int? IdSincronizacion { get; set; }
        public DateTime FechaError { get; set; }

        public virtual Sincronizacion IdSincronizacionNavigation { get; set; }
    }
}
