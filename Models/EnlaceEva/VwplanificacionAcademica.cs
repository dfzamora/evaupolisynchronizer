﻿using System;
using System.Collections.Generic;

namespace moodle.Models.EnlaceEva
{
    public partial class VwplanificacionAcademica
    {
        public string PeriodoLectivo { get; set; }
        public int CicloId { get; set; }
        public int CursoId { get; set; }
        public string Asignatura { get; set; }
        public int? DocenteId { get; set; }
        public string DocenteCorreo { get; set; }
        public string Turno { get; set; }
        public string NomeclaturaCurso { get; set; }
        public int RecintoId { get; set; }
        public string Recinto { get; set; }
        public int EscuelaId { get; set; }
        public string Escuela { get; set; }
        public int CarreraId { get; set; }
        public string Carrera { get; set; }
    }
}
