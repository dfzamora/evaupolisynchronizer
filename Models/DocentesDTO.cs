namespace Models
{
    public partial class DocentesDTO
    {
        public int CodigoDocente {get;set;}
        public string CorreoDocente {get;set;}
        public string Nombre {get;set;}

    }
}