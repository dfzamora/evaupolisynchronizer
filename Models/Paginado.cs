namespace Models
{

    public class Paginado
    {
        const int maxPageSize = 500;
        public int pagina { get; set; } = 1;

        private int _registroPorPagina = 10;
        public int registroPorPagina
        {
            get
            {
                return _registroPorPagina;
            }
            set
            {
                _registroPorPagina = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public int skip => (pagina - 1) * registroPorPagina;

        public string OrdenarPor { get; set; } //Columna de la tabla
        public string Orden { get; set; } //Corresponde al tipo de orden

    }



}

