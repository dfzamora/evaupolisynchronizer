using System.ComponentModel.DataAnnotations.Schema;

namespace moodle.Models.EnlaceEva
{
    public partial class PlanificacionAcademicaDTO
    {
        public string Asignatura { get; set; }
        public string NomeclaturaCurso { get; set; }
        public int CursoId { get; set; }
    }
}
