﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using moodle.Models.EnlaceEva;

namespace moodle.Database
{
    public partial class EnlaceEVAContext : DbContext
    {
        public EnlaceEVAContext()
        {
        }

        public EnlaceEVAContext(DbContextOptions<EnlaceEVAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CursoMigrado> CursoMigrado { get; set; }
        public virtual DbSet<EstudianteMigrado> EstudianteMigrado { get; set; }
        public virtual DbSet<InfoEquivalencia> InfoEquivalencia { get; set; }
        public virtual DbSet<LogError> LogError { get; set; }
        public virtual DbSet<Sincronizacion> Sincronizacion { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<VwcambiosDocentes> VwcambiosDocentes { get; set; }
        public virtual DbSet<VwestudiantesInscritosciclosactivos> VwestudiantesInscritosciclosactivos { get; set; }
        public virtual DbSet<VwinfoEquivalente> VwinfoEquivalente { get; set; }
        public virtual DbSet<VwperfilDocente> VwperfilDocente { get; set; }
        public virtual DbSet<VwperfilEstudiantes> VwperfilEstudiantes { get; set; }
        public virtual DbSet<VwplanificacionAcademica> VwplanificacionAcademica { get; set; }
        public virtual DbSet<VwreporteEstudianteNoMigrado> VwreporteEstudianteNoMigrado { get; set; }
        public virtual DbSet<VwreporteMigrados> VwreporteMigrados { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CursoMigrado>(entity =>
            {
                entity.HasKey(e => e.IdCursoMigrado);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdcursoEva).HasColumnName("IDCursoEva");

                entity.HasOne(d => d.IdSincronizacionNavigation)
                    .WithMany(p => p.CursoMigrado)
                    .HasForeignKey(d => d.IdSincronizacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CursoMigrado_Sincronizacion");
            });

            modelBuilder.Entity<EstudianteMigrado>(entity =>
            {
                entity.HasKey(e => e.IdEstudianteMigrado);

                entity.Property(e => e.Carnet)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdSincronizacionNavigation)
                    .WithMany(p => p.EstudianteMigrado)
                    .HasForeignKey(d => d.IdSincronizacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EstudianteMigrado_Sincronizacion");
            });

            modelBuilder.Entity<InfoEquivalencia>(entity =>
            {
                entity.HasKey(e => e.IdInfoEquivalencia);

                entity.HasIndex(e => e.IdCategoriaEva)
                    .HasName("IX_InfoEquivalencia")
                    .IsUnique();

                entity.HasIndex(e => new { e.RecintoId, e.CarreraId, e.EscuelaId })
                    .HasName("IX_InfoEquivalencia_1");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdCategoriaEva)
                    .HasColumnName("IdCategoriaEVA")
                    .HasComment("Este campo pertenece a la tabla CursoCategorias de EVA");

                entity.Property(e => e.IdPadreEva).HasColumnName("IdPadreEVA");

                entity.Property(e => e.NameEva)
                    .IsRequired()
                    .HasColumnName("NameEVA")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RamaEva)
                    .IsRequired()
                    .HasColumnName("RamaEVA")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogError>(entity =>
            {
                entity.HasKey(e => e.IdLogError);

                entity.Property(e => e.Error)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.FechaError)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TipoError)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasComment(@"Tipos de error:
Curso
Estudiante
Base de Datos
Conexion
No especificado");

                entity.HasOne(d => d.IdSincronizacionNavigation)
                    .WithMany(p => p.LogError)
                    .HasForeignKey(d => d.IdSincronizacion)
                    .HasConstraintName("FK_LogError_Sincronizacion");
            });

            modelBuilder.Entity<Sincronizacion>(entity =>
            {
                entity.HasKey(e => e.IdSincronizacion);

                entity.Property(e => e.EnProceso).HasColumnName("enProceso");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaDesde).HasColumnType("datetime");

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.Tipo).HasComment("0=Automatica 1=Manual");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Sincronizacion)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_Sincronizacion_Usuario");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsAdmin).HasColumnName("isAdmin");

                entity.Property(e => e.Pass)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario1)
                    .IsRequired()
                    .HasColumnName("Usuario")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwcambiosDocentes>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWCambiosDocentes");

                entity.Property(e => e.ApellidosAnterior)
                    .IsRequired()
                    .HasColumnName("apellidosAnterior")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.ApellidosNuevo)
                    .IsRequired()
                    .HasColumnName("apellidosNuevo")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoAnterior)
                    .IsRequired()
                    .HasColumnName("correoAnterior")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoNuevo)
                    .IsRequired()
                    .HasColumnName("correoNuevo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fechaModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.NombresAnterior)
                    .IsRequired()
                    .HasColumnName("nombresAnterior")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.NombresNuevo)
                    .IsRequired()
                    .HasColumnName("nombresNuevo")
                    .HasMaxLength(41)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwestudiantesInscritosciclosactivos>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWEstudiantesInscritosciclosactivos");

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnName("apellidos")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Carnet)
                    .IsRequired()
                    .HasColumnName("carnet")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ciclo)
                    .HasColumnName("ciclo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CicloId).HasColumnName("cicloID");

                entity.Property(e => e.CorreoEstudiante)
                    .HasColumnName("correoEstudiante")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CursoId).HasColumnName("cursoID");

                entity.Property(e => e.EstadoInscripcion)
                    .IsRequired()
                    .HasColumnName("estadoInscripcion")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEliminado)
                    .HasColumnName("fechaEliminado")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaEstado)
                    .HasColumnName("fechaEstado")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaInscripcion)
                    .HasColumnName("fechaInscripcion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaRetiro)
                    .HasColumnName("fechaRetiro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(201)
                    .IsUnicode(false);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasColumnName("nombres")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PerCodigo).HasColumnName("per_codigo");

                entity.Property(e => e.Regional)
                    .IsRequired()
                    .HasColumnName("regional")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionalId).HasColumnName("regionalID");
            });

            modelBuilder.Entity<VwinfoEquivalente>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWInfoEquivalente");

                entity.Property(e => e.Carrera)
                    .IsRequired()
                    .HasColumnName("carrera")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CarreraId).HasColumnName("carreraID");

                entity.Property(e => e.Escuela)
                    .HasColumnName("escuela")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EscuelaId).HasColumnName("escuelaID");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.IdCategoriaEva).HasColumnName("IdCategoriaEVA");

                entity.Property(e => e.IdPadreEva).HasColumnName("IdPadreEVA");

                entity.Property(e => e.NameEva)
                    .IsRequired()
                    .HasColumnName("NameEVA")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RamaEva)
                    .IsRequired()
                    .HasColumnName("RamaEVA")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RamaUpoli)
                    .HasColumnName("RamaUPOLI")
                    .HasMaxLength(352)
                    .IsUnicode(false);

                entity.Property(e => e.Recinto)
                    .IsRequired()
                    .HasColumnName("recinto")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecintoId).HasColumnName("recintoID");
            });

            modelBuilder.Entity<VwperfilDocente>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWPerfilDocente");

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnName("apellidos")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoDocente).HasColumnName("codigoDocente");

                entity.Property(e => e.Correo)
                    .HasColumnName("correo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Genero)
                    .IsRequired()
                    .HasColumnName("genero")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCompleto)
                    .HasMaxLength(104)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwperfilEstudiantes>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWPerfilEstudiantes");

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnName("apellidos")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Carnet)
                    .IsRequired()
                    .HasColumnName("carnet")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Carrera)
                    .IsRequired()
                    .HasColumnName("carrera")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Correo)
                    .HasColumnName("correo")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Departamento).HasColumnName("departamento");

                entity.Property(e => e.Escuela)
                    .HasColumnName("escuela")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Genero)
                    .IsRequired()
                    .HasColumnName("genero")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Nacionalidad).HasColumnName("nacionalidad");

                entity.Property(e => e.NombreCompleto)
                    .IsRequired()
                    .HasColumnName("nombreCompleto")
                    .HasMaxLength(201)
                    .IsUnicode(false);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasColumnName("nombres")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Recinto)
                    .IsRequired()
                    .HasColumnName("recinto")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwplanificacionAcademica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWPlanificacionAcademica");

                entity.Property(e => e.Asignatura)
                    .IsRequired()
                    .HasColumnName("asignatura")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Carrera)
                    .IsRequired()
                    .HasColumnName("carrera")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CarreraId).HasColumnName("carreraID");

                entity.Property(e => e.CicloId).HasColumnName("cicloID");

                entity.Property(e => e.CursoId).HasColumnName("cursoID");

                entity.Property(e => e.DocenteCorreo)
                    .HasColumnName("docenteCorreo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DocenteId).HasColumnName("docenteID");

                entity.Property(e => e.Escuela)
                    .HasColumnName("escuela")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EscuelaId).HasColumnName("escuelaID");

                entity.Property(e => e.NomeclaturaCurso)
                    .HasColumnName("nomeclaturaCurso")
                    .HasMaxLength(264)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodoLectivo)
                    .HasColumnName("periodoLectivo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Recinto)
                    .IsRequired()
                    .HasColumnName("recinto")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecintoId).HasColumnName("recintoID");

                entity.Property(e => e.Turno)
                    .HasColumnName("turno")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwreporteEstudianteNoMigrado>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWReporteEstudianteNoMigrado");

                entity.Property(e => e.Carnet)
                    .IsRequired()
                    .HasColumnName("carnet")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CicloId).HasColumnName("cicloID");

                entity.Property(e => e.CorreoEstudiante)
                    .HasColumnName("correoEstudiante")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CursoId).HasColumnName("cursoID");

                entity.Property(e => e.FechaEstado)
                    .HasColumnName("fechaEstado")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaInscripcion)
                    .HasColumnName("fechaInscripcion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(201)
                    .IsUnicode(false);

                entity.Property(e => e.NomeclaturaCurso)
                    .HasColumnName("nomeclaturaCurso")
                    .HasMaxLength(264)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwreporteMigrados>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWReporteMigrados");

                entity.Property(e => e.CicloId).HasColumnName("cicloID");

                entity.Property(e => e.CursoId).HasColumnName("cursoID");

                entity.Property(e => e.NomeclaturaCurso)
                    .HasColumnName("nomeclaturaCurso")
                    .HasMaxLength(264)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
