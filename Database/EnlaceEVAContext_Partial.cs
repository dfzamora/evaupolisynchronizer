using System;
using Microsoft.EntityFrameworkCore;
using moodle.Models.EnlaceEva;

namespace moodle.Database
{
    public partial class EnlaceEVAContext : DbContext
    {
        public virtual DbSet<ServerInfo> ServerInfo { get; set; }
        public virtual DbSet<VwestudiantesInscritosciclosactivos> VwestudiantesInscritosciclosactivosQuery { get; set; }
        public virtual DbSet<ConsolidadoEstudiantesDTO> ConsolidadoEstudiantes { get; set; }
        public virtual DbSet<VwperfilDocente> VwperfilDocenteQuery { get; set; }

    }
}