﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using moodle.Models.bitnami_moodle;

namespace moodle.Database
{
    public partial class MariaDBContext : DbContext
    {
        public MariaDBContext()
        {
        }

        public MariaDBContext(DbContextOptions<MariaDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Estudiantes> Estudiantes { get; set; }
        public virtual DbSet<MdlCourse> MdlCourse { get; set; }
        public virtual DbSet<Vwcambiosdocentes> Vwcambiosdocentes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
         
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estudiantes>(entity =>
            {
                entity.ToTable("estudiantes");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Apellidos)
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Correo)
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Estado)
                    .HasColumnType("char(1)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.IdcursoEva)
                    .HasColumnName("idcursoEva")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Nombres)
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");
            });

            modelBuilder.Entity<MdlCourse>(entity =>
            {
                entity.ToTable("mdl_course");

                entity.HasComment("Central course table");

                entity.HasIndex(e => e.Category)
                    .HasName("mdl_cour_cat_ix");

                entity.HasIndex(e => e.Idnumber)
                    .HasName("mdl_cour_idn_ix");

                entity.HasIndex(e => e.Shortname)
                    .HasName("mdl_cour_sho_ix");

                entity.HasIndex(e => e.Sortorder)
                    .HasName("mdl_cour_sor_ix");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Cacherev)
                    .HasColumnName("cacherev")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Calendartype)
                    .IsRequired()
                    .HasColumnName("calendartype")
                    .HasColumnType("varchar(30)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Completionnotify).HasColumnName("completionnotify");

                entity.Property(e => e.Defaultgroupingid)
                    .HasColumnName("defaultgroupingid")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Enablecompletion).HasColumnName("enablecompletion");

                entity.Property(e => e.Enddate)
                    .HasColumnName("enddate")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Format)
                    .IsRequired()
                    .HasColumnName("format")
                    .HasColumnType("varchar(21)")
                    .HasDefaultValueSql("'topics'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasColumnName("fullname")
                    .HasColumnType("varchar(254)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Groupmode)
                    .HasColumnName("groupmode")
                    .HasColumnType("smallint(4)");

                entity.Property(e => e.Groupmodeforce)
                    .HasColumnName("groupmodeforce")
                    .HasColumnType("smallint(4)");

                entity.Property(e => e.Idnumber)
                    .IsRequired()
                    .HasColumnName("idnumber")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Lang)
                    .IsRequired()
                    .HasColumnName("lang")
                    .HasColumnType("varchar(30)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Legacyfiles)
                    .HasColumnName("legacyfiles")
                    .HasColumnType("smallint(4)");

                entity.Property(e => e.Marker)
                    .HasColumnName("marker")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Maxbytes)
                    .HasColumnName("maxbytes")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Newsitems)
                    .HasColumnName("newsitems")
                    .HasColumnType("mediumint(5)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Relativedatesmode).HasColumnName("relativedatesmode");

                entity.Property(e => e.Requested).HasColumnName("requested");

                entity.Property(e => e.Shortname)
                    .IsRequired()
                    .HasColumnName("shortname")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Showgrades)
                    .HasColumnName("showgrades")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Showreports)
                    .HasColumnName("showreports")
                    .HasColumnType("smallint(4)");

                entity.Property(e => e.Sortorder)
                    .HasColumnName("sortorder")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Startdate)
                    .HasColumnName("startdate")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Summary)
                    .HasColumnName("summary")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Summaryformat)
                    .HasColumnName("summaryformat")
                    .HasColumnType("tinyint(2)");

                entity.Property(e => e.Theme)
                    .IsRequired()
                    .HasColumnName("theme")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Timecreated)
                    .HasColumnName("timecreated")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Timemodified)
                    .HasColumnName("timemodified")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Visible)
                    .IsRequired()
                    .HasColumnName("visible")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Visibleold)
                    .IsRequired()
                    .HasColumnName("visibleold")
                    .HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<Vwcambiosdocentes>(entity =>
            {
                entity.ToTable("vwcambiosdocentes");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ApellidosAnterior)
                    .HasColumnName("apellidosAnterior")
                    .HasColumnType("varchar(41)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ApellidosNuevo)
                    .HasColumnName("apellidosNuevo")
                    .HasColumnType("varchar(41)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CorreoAnterior)
                    .HasColumnName("correoAnterior")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CorreoNuevo)
                    .HasColumnName("correoNuevo")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CursoId).HasColumnType("bigint(20)");

                entity.Property(e => e.Estado)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("'N'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NombresAnterior)
                    .HasColumnName("nombresAnterior")
                    .HasColumnType("varchar(41)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NombresNuevo)
                    .HasColumnName("nombresNuevo")
                    .HasColumnType("varchar(41)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
