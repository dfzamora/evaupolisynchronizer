using Microsoft.EntityFrameworkCore;
using moodle.Models.bitnami_moodle;

namespace moodle.Database
{
    public partial class MariaDBContext : DbContext
    {
        public virtual DbSet<InscribirCursoEVA> InscribirCursoEVA { get; set; }
        public virtual DbSet<InscribirEstudianteEVA> InscribirEstudianteEVA { get; set; }
        public virtual DbSet<CategoryEVA> CategoryEVA { get; set; }


    }
}