using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models;
using moodle.Database;
using moodle.Models.bitnami_moodle;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface IInfoEnlaceService
    {
        Task GuardarEquivalencia(PostInfoEnlace data);
        Task<List<CategoryEVA>> ObtenerCategoriasEva();
        Task<List<CategoriaUpoliDTO>> ObtenerCategoriasUpoli();

        Task EliminarEquivalencia(int id);
        Task<Paginacion<VwinfoEquivalente>> ObtenerEquivalencias(Paginado paginado);
    }

    public class InfoEnlaceService : IInfoEnlaceService
    {
        private readonly EnlaceEVAContext _db;
        private readonly MariaDBContext _mariaDB;
        public InfoEnlaceService(EnlaceEVAContext db, MariaDBContext mariaDB)
        {
            _db = db;
            _mariaDB = mariaDB;
        }

        public async Task EliminarEquivalencia(int id)
        {
            InfoEquivalencia original = await _db.InfoEquivalencia.FindAsync(id);

            if (original != null)
            {
                _db.InfoEquivalencia.Remove(original);
                await _db.SaveChangesAsync();
            }
            else throw new NotFoundException();
        }

        public async Task GuardarEquivalencia(PostInfoEnlace data)
        {
            InfoEquivalencia i = new InfoEquivalencia()
            {
                RecintoId = data.categoryUpoli.RecintoId,
                EscuelaId = data.categoryUpoli.EscuelaId,
                CarreraId = data.categoryUpoli.CarreraId,
                IdCategoriaEva = data.categoryEva.IdEVA,
                IdPadreEva = data.categoryEva.parentEVA,
                NameEva = data.categoryEva.NameEVA,
                RamaEva = data.categoryEva.RamaEVA
            };

            await _db.InfoEquivalencia.AddAsync(i);
            await _db.SaveChangesAsync();
        }

        public async Task<List<CategoryEVA>> ObtenerCategoriasEva()
        {

            List<CategoryEVA> categoriasEva = await _mariaDB
                                                    .CategoryEVA
                                                    .FromSqlRaw("CALL Categoria()")
                                                    .ToListAsync();
            return categoriasEva
                            .Where(w => !_db.InfoEquivalencia.Any(a => a.IdCategoriaEva == w.IdEVA))
                            .ToList();
        }

        public async Task<List<CategoriaUpoliDTO>> ObtenerCategoriasUpoli()
        {
            List<CategoriaUpoliDTO> categorias = await _db.VwplanificacionAcademica
                            .Select(s => new CategoriaUpoliDTO
                            {
                                EscuelaId = s.EscuelaId,
                                Escuela = s.Escuela,
                                RecintoId = s.RecintoId,
                                Recinto = s.Recinto,
                                CarreraId = s.CarreraId,
                                Carrera = s.Carrera,
                                rama = string.Format("{0}/{1}/{2}", s.Recinto, s.Escuela, s.Carrera)
                            })
                            .Where(w => !_db.InfoEquivalencia.Any(w1 => w1.CarreraId == w.CarreraId && w1.EscuelaId == w.EscuelaId && w1.RecintoId == w.RecintoId))
                            .Distinct()
                            .ToListAsync();

            return categorias
                    .OrderBy(o => o.Recinto)
                    .ThenBy(t => t.Escuela)
                    .ThenBy(t => t.Carrera)
                    .ToList();
        }

        public async Task<Paginacion<VwinfoEquivalente>> ObtenerEquivalencias(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();
            IQueryable<VwinfoEquivalente> query = _db.VwinfoEquivalente;

            return await p.paginar<VwinfoEquivalente>(query, paginado);


        }
    }


}

