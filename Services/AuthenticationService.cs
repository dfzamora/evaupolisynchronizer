using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using moodle.Database;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface IAuthenticateService
    {
        Task<Usuario> Authenticate(Usuario user);
    }

    public class AuthenticationService : IAuthenticateService
    {
        private readonly IConfiguration configuration;
        private readonly EnlaceEVAContext db;
        public AuthenticationService(IConfiguration configuration, EnlaceEVAContext db)
        {
            this.configuration = configuration;
            this.db = db;
        }

        public async Task<Usuario> Authenticate(Usuario usuario)
        {
            string pass = Security.encryptPass(usuario.Pass);

            Usuario user = await db.Usuario.FirstOrDefaultAsync(w => w.Usuario1 == usuario.Usuario1);
            if (user != null)
            {
                if (user.Pass == pass)
                {
                    GenerarToken(user);
                    return user;
                }
                else
                {
                    return new Usuario() { Pass = "not-found" };
                }

            }
            else
            {
                return new Usuario() { Usuario1 = "not-found" };
            }


        }

        private void GenerarToken(Usuario user)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(configuration["Jwt:Key"]);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Usuario1.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.IdUsuario.ToString()),
                    new Claim(ClaimTypes.Role, user.IsAdmin?"Administrador":""),
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            user.token = tokenHandler.WriteToken(token);
            user.Pass = ""; //se envía vacío por seguridad
        }

    }


}

