
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;
using moodle.Database;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface IReporteService
    {
        Task<Paginacion<EstudiantesDTO>> ObtenerEstudiantesNoValidos(Paginado paginado);
        Task<Paginacion<DocentesDTO>> ObtenerDocentesNoValidos(Paginado paginado);
        Task<Paginacion<VwreporteMigrados>> ObtenerEstudiantesMigrados(Paginado paginado);
        Task<ConsolidadoEstudiantesDTO> ObtenerTotalEstudiantesMigrados();
        Task<Paginacion<LogError>> ObtenerErroresServidor(Paginado paginado);
        Task<Paginacion<VwreporteEstudianteNoMigrado>> ObtenerDetalleEstudiantesNoMigrados(int cicloId, int cursoId, Paginado paginado);
    }

    public class ReporteService : IReporteService
    {
        private readonly EnlaceEVAContext _db;

        public ReporteService(EnlaceEVAContext db)
        {
            _db = db;

        }

        public async Task<Paginacion<VwreporteEstudianteNoMigrado>> ObtenerDetalleEstudiantesNoMigrados(int cicloId, int cursoId, Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();
            IQueryable<VwreporteEstudianteNoMigrado> query = _db.VwreporteEstudianteNoMigrado
            .Where(w => w.CicloId == cicloId && w.CursoId == cursoId);

            return await p.paginar(query, paginado);
        }

        public async Task<Paginacion<DocentesDTO>> ObtenerDocentesNoValidos(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();

            IQueryable<DocentesDTO> query = _db.VwperfilDocenteQuery
                                                .FromSqlRaw($"SELECT * FROM VWPerfilDocente WHERE correo not like '%@%' and correo not like '%{Referencias.upoliMailDomain}'")
                                                .Select(s => new DocentesDTO
                                                {
                                                    CorreoDocente = s.Correo,
                                                    CodigoDocente = s.CodigoDocente,
                                                    Nombre = s.NombreCompleto
                                                });

            return await p.paginar(query, paginado);
        }

        public async Task<Paginacion<LogError>> ObtenerErroresServidor(Paginado paginado)
        {
            IQueryable<LogError> query = _db.LogError.Where(w => w.TipoError == Referencias.ERRORSERVIDOR);
            PaginarExtension p = new PaginarExtension();

            return await p.paginar<LogError>(query, paginado);
        }

        public async Task<Paginacion<VwreporteMigrados>> ObtenerEstudiantesMigrados(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();
            IQueryable<VwreporteMigrados> query = _db.VwreporteMigrados;

            return await p.paginar(query, paginado);
        }

        public async Task<Paginacion<EstudiantesDTO>> ObtenerEstudiantesNoValidos(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();

            IQueryable<EstudiantesDTO> query = _db.VwestudiantesInscritosciclosactivosQuery.FromSqlRaw($"SELECT * FROM VWEstudiantesInscritosciclosactivos WHERE correoEstudiante not like '%@%' and correoEstudiante not like '%{Referencias.upoliMailDomain}'")
                    .Select(s => new EstudiantesDTO
                    {
                        Nombre = s.Nombre,
                        Carnet = s.Carnet,
                        CorreoEstudiante = s.CorreoEstudiante
                    }).Distinct();

            return await p.paginar(query, paginado);
        }

        public async Task<ConsolidadoEstudiantesDTO> ObtenerTotalEstudiantesMigrados()
        {
            ConsolidadoEstudiantesDTO query = await _db.ConsolidadoEstudiantes.FromSqlRaw(@" 
                                                            SELECT
                                                                COUNT(DISTINCT (VI.carnet)) Cantidad
                                                            FROM VWEstudiantesInscritosciclosactivos AS VI 
                                                                INNER JOIN EstudianteMigrado AS E ON VI.carnet = E.Carnet 
                                                                INNER JOIN VWPlanificacionAcademica AS PA ON VI.cursoId = PA.cursoID	
                                                            ").SingleOrDefaultAsync();
            return query;
        }
    }
}