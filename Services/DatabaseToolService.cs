using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using moodle.Database;
using moodle.Models.bitnami_moodle;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface IDataBaseToolService
    {
        Task<ServerInfo> ObtenerHoraSQL();
    }

    public class DataBaseToolService : IDataBaseToolService
    {
        private readonly EnlaceEVAContext _db;
        public DataBaseToolService(EnlaceEVAContext db)
        {
            _db = db;
        }

        public async Task<ServerInfo> ObtenerHoraSQL()
        {
            return await _db.ServerInfo.FromSqlRaw<ServerInfo>("SELECT GETDATE() Fecha").SingleOrDefaultAsync();

        }
    }


}

