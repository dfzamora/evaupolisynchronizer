using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using moodle.Database;
using moodle.Models.bitnami_moodle;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface ICursoService
    {
        Task<InscribirCursoEVA> GuardarCurso(VwplanificacionAcademica c);
        Task<List<PlanificacionAcademicaDTO>> ObtenerPlanificacionAcademica();
    }

    public class CursoService : ICursoService
    {
        private readonly EnlaceEVAContext db;
        private readonly MariaDBContext mariaDB;
        public CursoService(EnlaceEVAContext db, MariaDBContext mariaDB)
        {
            this.db = db;
            this.mariaDB = mariaDB;
        }

        public async Task<InscribirCursoEVA> GuardarCurso(VwplanificacionAcademica c)
        {


            InscribirCursoEVA data = await InscribirCursoEVA(c);

            await GuardarCursoMigrado(data, c.CursoId);

            await GuardarCursosEVA(c.CursoId, data.IDCursoEva);


            return data;
        }

        private async Task<InscribirCursoEVA> InscribirCursoEVA(VwplanificacionAcademica c)
        {
            List<InscribirCursoEVA> data1 = await mariaDB.InscribirCursoEVA
                                       .FromSqlRaw("CALL CreateCurso ({0},{1},{2})",
                                       c.Asignatura,
                                       c.NomeclaturaCurso,
                                       c.CursoId
                                       ).ToListAsync();

            return data1.FirstOrDefault();

        }

        private async Task GuardarCursoMigrado(InscribirCursoEVA inscripcion, int CursoId)
        {
            CursoMigrado cm = new CursoMigrado();
            cm.IdcursoEva = inscripcion.IDCursoEva;
            cm.CursoId = CursoId;
            cm.Categoria = inscripcion.category.Value;
            cm.FechaCreacion = DateTime.Now;

            await db.CursoMigrado.AddAsync(cm);
            await db.SaveChangesAsync();
        }
        private async Task GuardarCursosEVA(int CursoId, long IDCursoEva)
        {
            List<VwestudiantesInscritosciclosactivos> estudiantes = await db.VwestudiantesInscritosciclosactivos.Where(w => w.CursoId == CursoId && w.CorreoEstudiante.Contains("@")).ToListAsync();

            foreach (var estudiante in estudiantes)
            {
                await mariaDB.InscribirEstudianteEVA
                                       .FromSqlRaw("CALL EnrollUserEVA ({0},{1},{2},{3})",
                                           IDCursoEva,
                                           estudiante.CorreoEstudiante,
                                           estudiante.Nombres,
                                           estudiante.Apellidos
                                       ).ToListAsync();
            }
        }

        public async Task<List<PlanificacionAcademicaDTO>> ObtenerPlanificacionAcademica()
        {
            List<PlanificacionAcademicaDTO> data = await db
                        .VwplanificacionAcademica
                        .Select(s => new PlanificacionAcademicaDTO
                        {
                            Asignatura = s.Asignatura,
                            NomeclaturaCurso = s.NomeclaturaCurso,
                            CursoId = s.CursoId
                        })
                        .Distinct()
                        .OrderBy(o => o.NomeclaturaCurso).ToListAsync();

            return data;
        }


    }


}

