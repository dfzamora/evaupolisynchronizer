using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using services;
using Microsoft.Extensions.Configuration;
using System;
using Models;


public class SyncroBackgroundService : BackgroundService
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly IConfiguration _configuration;
    private readonly ISyncronizerAlgoritmService _syncronizerAlgoritmService;

    public SyncroBackgroundService(IServiceScopeFactory scopeFactory, IConfiguration configuration, IServiceProvider serviceProvider)
    {
        _scopeFactory = scopeFactory;
        _configuration = configuration;
        _syncronizerAlgoritmService = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ISyncronizerAlgoritmService>();
    }
    protected async override Task ExecuteAsync(CancellationToken stoppingToken)
    {

        using (var scope = _scopeFactory.CreateScope())
        {
            int time = int.Parse(_configuration["Sincronizar:time"]);
            for (var i = 1; !stoppingToken.IsCancellationRequested; i++)
            {
                await LlamarSincronizador();
                await Task.Delay(time);
            }
        }
    }

    private async Task LlamarSincronizador()
    {
        try
        {
            await _syncronizerAlgoritmService.Sincronizar(new SyncronizacionSaveDTO());
        }
        catch (Exception)
        {
            //En esta exception no se hace nada para que el proceso no se detenga
        }
    }
}
