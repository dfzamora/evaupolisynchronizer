using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models;
using moodle.Database;
using moodle.Models.bitnami_moodle;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface IUsuarioService
    {
        Task CrearUsuario(Usuario usuario);
        Task CambiarContrasena(Usuario usuario);
        Task EliminarUsuario(int id);
        Task<Paginacion<Usuario>> ObtenerUsuarios(Paginado paginado);
        Task ActualizarUsuario(Usuario usuario);
    }

    public class UsuarioService : IUsuarioService
    {
        private readonly EnlaceEVAContext db;
        public UsuarioService(EnlaceEVAContext db)
        {
            this.db = db;
        }

        public async Task ActualizarUsuario(Usuario usuario)
        {
          Usuario original = await db.Usuario.FirstOrDefaultAsync(f => f.IdUsuario == usuario.IdUsuario);

            if (original != null)
            {
                original.Usuario1 = usuario.Usuario1;
                original.IsAdmin = usuario.IsAdmin;

                db.Update(original);
                await db.SaveChangesAsync();
            }
            else throw new NotFoundException();

        }

        public async Task CambiarContrasena(Usuario usuario)
        {
            Usuario original = await db.Usuario.FirstOrDefaultAsync(f => f.IdUsuario == usuario.IdUsuario);

            if (original != null)
            {
                original.Pass = Security.encryptPass(usuario.Pass);

                db.Update(original);
                await db.SaveChangesAsync();
            }
            else throw new NotFoundException();

        }

        public async Task CrearUsuario(Usuario usuario)
        {
            if (await db.Usuario.AnyAsync(a => a.Usuario1 == usuario.Usuario1)) throw new DuplicatedException("El usuario que está intentando crear ya existe.");

            Usuario user = new Usuario()
            {
                Usuario1 = usuario.Usuario1,
                Pass = Security.encryptPass(usuario.Pass),
                FechaCreacion = DateTime.Now,
                IsAdmin = usuario.IsAdmin
            };

            await db.Usuario.AddAsync(user);
            await db.SaveChangesAsync();
        }

        public async Task EliminarUsuario(int id)
        {
            Usuario user = await db.Usuario.FindAsync(id);
            if (user != null)
            {
                db.Usuario.Remove(user);
                await db.SaveChangesAsync();
            }
            else throw new NotFoundException();
        }

        public async Task<Paginacion<Usuario>> ObtenerUsuarios(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();
            IQueryable<Usuario> query = db.Usuario;

            return await p.paginar<Usuario>(query, paginado);
        }
    }
}

