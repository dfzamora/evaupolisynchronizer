using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using moodle.Database;
using moodle.Models.bitnami_moodle;
using moodle.Models.EnlaceEva;
using Util;

namespace services
{
    public interface ISyncronizerAlgoritmService
    {
        Task<SyncronizerDTO> Sincronizar(SyncronizacionSaveDTO sincroSave);
        Task<Paginacion<SyncronizacionDTO>> ObtenerSincronizaciones(Paginado paginado);
        Task<Paginacion<LogError>> ObtenerErroresSincronizacion(int id, Paginado paginado);

    }

    public class SyncronizerAlgoritmService : ISyncronizerAlgoritmService
    {
        private readonly EnlaceEVAContext _db;
        private readonly MariaDBContext _mariaDB;
        private readonly IConfiguration _configuration;
        private readonly IDataBaseToolService _dataBaseToolService;

        public SyncronizerAlgoritmService(EnlaceEVAContext db, MariaDBContext mariaDB, IConfiguration configuration, IDataBaseToolService dataBaseToolService)
        {
            _configuration = configuration;
            _db = db;
            _mariaDB = mariaDB;
            _dataBaseToolService = dataBaseToolService;
        }
        public async Task<SyncronizerDTO> Sincronizar(SyncronizacionSaveDTO sincroSave)
        {

            Sincronizacion sincro = null;

            try
            {
                await VerificarSincronizacionEnProgreso();

                Sincronizacion ultimaSincroCompleted = await _db.Sincronizacion.OrderByDescending(t => t.IdSincronizacion).FirstOrDefaultAsync(f => f.Fecha != null);

                if (sincroSave.FechaDesde != null)
                    ultimaSincroCompleted.Fecha = sincroSave.FechaDesde;

                ServerInfo serverInfo = await _dataBaseToolService.ObtenerHoraSQL();

                sincro = await GuardarSincronizacion(sincroSave, serverInfo);

                List<PlanificacionAcademicaDocenteDTO> cursosMigrar = await ObtenerCursosMigrar();

                await RecorrerValidarCursosMigrar(cursosMigrar, sincro, ultimaSincroCompleted);

                await AlmacenarCambioDocentes(ultimaSincroCompleted?.Fecha);

                await FinalizarSincronizacion(sincro);

                return new SyncronizerDTO
                {
                    IdSincronizacion = sincro.IdSincronizacion,
                    cursosMigrar = sincro.CursoMigrado.Count,
                    Estudiantes = sincro.EstudianteMigrado.Count,
                    Errores = sincro.LogError.Count,
                    Fecha = sincro.Fecha
                };

            }
            catch (Exception ex)
            {
                if (ex is SincronizationInProgressException)
                {
                    throw ex;
                }
                else
                {
                    EliminarFechaSincronizacion(sincro);
                    await GuardarError(new LogErrorDTO(sincro != null ? sincro.IdSincronizacion : (int?)null)
                    {
                        TipoError = Referencias.ERRORSERVIDOR,
                        Error = string.Format("ERROR: {0}", ex.GetMessage()),
                    });

                    throw new SincronizerServerErrorException();

                }
            }


        }

        private async Task VerificarSincronizacionEnProgreso()
        {
            Sincronizacion ultimaSincroInProgess = await _db.Sincronizacion.OrderByDescending(t => t.IdSincronizacion).FirstOrDefaultAsync(f => f.EnProceso);
            if (ultimaSincroInProgess != null)
            {
                if (await verificarSincronizacionCaida(ultimaSincroInProgess))
                {
                    LogErrorDTO error = new LogErrorDTO(ultimaSincroInProgess.IdSincronizacion)
                    {
                        TipoError = Referencias.ErrorInesperado,
                        Error = "Ocurrió un error no controlado con la aplicación, posiblemente se haya finalizado su ejecución de manera inesperada, por favor verifique el Visor de Eventos de Windows"
                    };

                    await GuardarError(error);
                    await FinalizarSincronizacion(ultimaSincroInProgess);


                }
                else
                {
                    throw new SincronizationInProgressException(ultimaSincroInProgess);

                }
            }
        }

        private async Task<bool> verificarSincronizacionCaida(Sincronizacion ultima)
        {
            EstudianteMigrado ultimoEstudiante = ultima.EstudianteMigrado.LastOrDefault();
            CursoMigrado ultimoCurso = ultima.CursoMigrado.LastOrDefault();
            int timesOut = int.Parse(_configuration["Sincronizar:time"]);
            ServerInfo horaActual = await _dataBaseToolService.ObtenerHoraSQL();

            if (SincronizacionTimeOut(ultima, horaActual, timesOut))
            {
                if (ultimoEstudiante == null)
                {
                    if (ultimoCurso == null)
                    {
                        return true;
                    }
                    else
                        return CursoTimeOut(ultimoCurso, horaActual, timesOut);
                }
                else if (ultimoCurso == null)
                {
                    return EstudianteTimeOut(ultimoEstudiante, horaActual, timesOut);
                }
                else
                {
                    return (CursoTimeOut(ultimoCurso, horaActual, timesOut) || EstudianteTimeOut(ultimoEstudiante, horaActual, timesOut));
                }

            }
            else { return false; }
        }

        private bool EstudianteTimeOut(EstudianteMigrado ultimoEstudiante, ServerInfo horaActual, int timesOut)
        {
            return tiempoEsperaAgotado(ultimoEstudiante.FechaCreacion, horaActual.Fecha, timesOut);
        }

        private bool CursoTimeOut(CursoMigrado ultimoCurso, ServerInfo horaActual, int timesOut)
        {
            return tiempoEsperaAgotado(ultimoCurso.FechaCreacion, horaActual.Fecha, timesOut);
        }

        private bool SincronizacionTimeOut(Sincronizacion ultimaSincronizacion, ServerInfo horaActual, int timesOut)
        {
            return tiempoEsperaAgotado(ultimaSincronizacion.FechaInicio, horaActual.Fecha, timesOut);
        }

        private bool tiempoEsperaAgotado(DateTime fechaAnterior, DateTime fechaActual, int maxTimeOut)
        {
            double diferenciaTiempo = fechaActual.Subtract(fechaAnterior).TotalMilliseconds;

            return diferenciaTiempo > maxTimeOut;
        }

        private async Task FinalizarSincronizacion(Sincronizacion sincro)
        {
            sincro.EnProceso = false;
            sincro.FechaFin = (await _dataBaseToolService.ObtenerHoraSQL()).Fecha;
            _db.Update(sincro);
            await _db.SaveChangesAsync();
        }

        private void EliminarFechaSincronizacion(Sincronizacion sincro)
        {
            if (sincro != null)
            {
                sincro.Fecha = null;
                sincro.EnProceso = false;
                _db.Update(sincro);
            }
        }

        private async Task AlmacenarCambioDocentes(DateTime? fecha)
        {
            List<Vwcambiosdocentes> docentesCambiados = await ObtenerDocentesCambiados(fecha);

            if (docentesCambiados.Any())
            {
                await _mariaDB.Vwcambiosdocentes.AddRangeAsync(
                      docentesCambiados
                  );

                await _mariaDB.SaveChangesAsync();
            }

            await _mariaDB.Database.ExecuteSqlRawAsync("CALL cambiodocentes()");
        }

        private async Task<List<Vwcambiosdocentes>> ObtenerDocentesCambiados(DateTime? fecha)
        {
            return await _db.VwcambiosDocentes
                                           .Where(w => w.FechaModificacion > fecha || fecha == null)
                                           .Select(s => new Vwcambiosdocentes
                                           {
                                               CursoId = s.CursoId,
                                               NombresAnterior = s.NombresAnterior,
                                               ApellidosAnterior = s.ApellidosAnterior,
                                               CorreoAnterior = s.CorreoAnterior,
                                               NombresNuevo = s.NombresNuevo,
                                               ApellidosNuevo = s.ApellidosNuevo,
                                               CorreoNuevo = s.CorreoNuevo,
                                               Estado = "N"
                                           })
                                           .ToListAsync();
        }

        private async Task RecorrerValidarCursosMigrar(List<PlanificacionAcademicaDocenteDTO> cursosMigrar, Sincronizacion sincro, Sincronizacion ultimaSincro)
        {
            foreach (PlanificacionAcademicaDocenteDTO item in cursosMigrar)
            {

                InfoEquivalencia tieneEquivalencia = await _db.InfoEquivalencia
                                                            .FirstOrDefaultAsync(f => f.CarreraId == item.planificaciones.CarreraId && f.EscuelaId == item.planificaciones.EscuelaId && f.RecintoId == item.planificaciones.RecintoId);

                if (tieneEquivalencia != null)
                {
                    CursoMigrado estaMigrado = await _db.CursoMigrado
                                                    .FirstOrDefaultAsync(f => f.CursoId == item.planificaciones.CursoId);

                    CursoMigrado cm = null;

                    if (estaMigrado == null)
                    {
                        if (item.docentes.Correo.Contains("@"))
                        {
                            if (item.docentes.Correo.EndsWith(Referencias.upoliMailDomain))
                            {
                                InscribirCursoEVA data = null;
                                try
                                {
                                    data = await InscribirCursoEVA(item, tieneEquivalencia.IdCategoriaEva);
                                }
                                catch (Exception ex)
                                {
                                    await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                                    {
                                        TipoError = Referencias.ErrorDBMoodle,
                                        Error = string.Format("Error al intentar crear el curso {1} en EVA, respuesta: {0}", ex.Message, item.planificaciones.NomeclaturaCurso),
                                    });
                                    continue;
                                }

                                if (string.IsNullOrEmpty(data.error))
                                {
                                    cm = await AlmacenarCursoMigrado(data, item, tieneEquivalencia, sincro);

                                    List<VwestudiantesInscritosciclosactivos> estudiantes = await ObtenerEstudiantesActivosCiclo(item.planificaciones.CursoId);

                                    await GuardarEstudiantesNoValidos(estudiantes, sincro.IdSincronizacion);

                                    await GuardarEstudiantesValidos(estudiantes, sincro.IdSincronizacion, cm);
                                }
                                else
                                {
                                    await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                                    {
                                        TipoError = Referencias.ErrorCursoDBMoodle,
                                        Error = string.Format("Error al intentar crear el curso {1} en EVA, respuesta: {0}", data.error, item.planificaciones.NomeclaturaCurso),

                                    });
                                    continue;
                                }

                            }
                            else
                            {
                                await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                                {
                                    TipoError = Referencias.ErrorCorreoDocente,
                                    Error = string.Format("El correo {1} del docente {0} no pertenece a una cuenta institucional de UPOLI, error al intentar crear el curso {2}", item.docentes.NombreCompleto, item.docentes.Correo, item.planificaciones.NomeclaturaCurso),
                                });
                                continue;
                            }

                        }
                        else
                        {
                            await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                            {
                                TipoError = Referencias.ErrorCorreoDocente,
                                Error = string.Format("No se pudo crear el curso {0} debibo a que el docente {1} no posee un correo válido", item.planificaciones.NomeclaturaCurso, item.docentes.NombreCompleto),
                            });
                            continue;
                        }
                    }
                    else
                    {
                        cm = estaMigrado;

                        List<VwestudiantesInscritosciclosactivos> estudiantes = await ObtenerEstudiantesEstadoMayorUltimaSincronizacion(item.planificaciones.CursoId, ultimaSincro.Fecha);
                        await RecorrerEstudiantesInscritos(estudiantes, cm, sincro, item);
                    }

                }
                else
                {
                    await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                    {
                        TipoError = Referencias.ErrorCurso,
                        Error = string.Format("El curso {3} se encuentra en la rama {0}/{1}/{2} la cual no se encuentra enlazada a ninguna rama de EVA", item.planificaciones.Recinto, item.planificaciones.Escuela, item.planificaciones.Carrera, item.planificaciones.NomeclaturaCurso),
                    });
                    continue;
                }
            }
        }

        private async Task RecorrerEstudiantesInscritos(List<VwestudiantesInscritosciclosactivos> estudiantes, CursoMigrado cm, Sincronizacion sincro, PlanificacionAcademicaDocenteDTO item)
        {
            foreach (VwestudiantesInscritosciclosactivos estudiante in estudiantes)
            {
                if (estudiante.CorreoEstudiante.Contains("@"))
                {
                    if (estudiante.CorreoEstudiante.EndsWith(Referencias.upoliMailDomain))
                    {
                        try
                        {
                            InscribirEstudianteEVA inscripcionEstudiante = await InscribirEstudianteEVA(estudiante, cm.IdcursoEva);

                            if (string.IsNullOrEmpty(inscripcionEstudiante.error))
                            {
                                await AlmacenarEstudianteMigrado(cm, estudiante, sincro.IdSincronizacion);
                            }
                            else
                            {
                                await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                                {
                                    TipoError = Referencias.ErrorEstudianteDBMoodle,
                                    Error = string.Format("No se pudo insertar el estudiante {0} con correo {1}, carnet {3}, se obtuvo el error: {2} ", estudiante.Nombre, estudiante.CorreoEstudiante, inscripcionEstudiante.error, estudiante.Carnet),
                                });
                                continue;
                            }
                        }
                        catch (Exception ex)
                        {
                            await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                            {
                                TipoError = Referencias.ErrorDBMoodle,
                                Error = $"No se pudo insertar el estudiante {(ex.Message.Contains(Referencias.ErrorDuplicateEntry) ? "Duplicado" : "")} {estudiante.Nombre} con correo {estudiante.CorreoEstudiante}, carnet {estudiante.Carnet} en la BD Moodle, para el curso {item.planificaciones.NomeclaturaCurso}"
                            });

                            continue;
                        }
                    }
                    else
                    {
                        await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                        {
                            TipoError = Referencias.ErrorEstudiante,
                            Error = string.Format("El estudiante {0} no posee un correo institucional de UPOLI, se encontró el correo {1}", estudiante.Nombre, estudiante.CorreoEstudiante),

                        });
                        continue;
                    }
                }
                else
                {
                    await GuardarError(new LogErrorDTO(sincro.IdSincronizacion)
                    {
                        TipoError = Referencias.ErrorEstudiante,
                        Error = $"El estudiante {estudiante.Nombre} no posee correo",
                    });
                    continue;
                }

            }

        }

        private async Task<List<VwestudiantesInscritosciclosactivos>> ObtenerEstudiantesEstadoMayorUltimaSincronizacion(int cursoId, DateTime? fecha)
        {
            return await _db
                            .VwestudiantesInscritosciclosactivos
                            .Where(w => w.CursoId == cursoId && w.FechaEstado > fecha)
                            .ToListAsync();
        }

        private async Task GuardarEstudiantesValidos(List<VwestudiantesInscritosciclosactivos> estudiantes, int idSincronizacion, CursoMigrado cm)
        {

            List<VwestudiantesInscritosciclosactivos> estudiantesValidos = estudiantes.Where(w => w.CorreoEstudiante.Contains("@") || w.CorreoEstudiante.EndsWith(Referencias.upoliMailDomain)).ToList();

            List<Estudiantes> e = estudiantesValidos
                        .Select(s => new Estudiantes()
                        {
                            IdcursoEva = cm.IdcursoEva,
                            Correo = s.CorreoEstudiante,
                            Nombres = s.Nombres,
                            Apellidos = s.Apellidos,
                            Estado = s.EstadoInscripcion
                        }
                        ).ToList();

            await _mariaDB.Estudiantes.AddRangeAsync(e);
            await _mariaDB.SaveChangesAsync();

            await MatricularEstudiantesMigradosEva(estudiantesValidos, cm);
        }

        private async Task MatricularEstudiantesMigradosEva(List<VwestudiantesInscritosciclosactivos> estudiantesValidos, CursoMigrado cm)
        {
            bool guardaSinError = true;
            IEnumerable<EstudianteMigrado> em = estudiantesValidos
                        .Select(s => new EstudianteMigrado()
                        {
                            CursoId = cm.CursoId,
                            Carnet = s.Carnet,
                            FechaCreacion = DateTime.Now,
                            IdSincronizacion = cm.IdSincronizacion,
                            Estado = s.EstadoInscripcion
                        });

            try
            {
                int estMasivo = await _mariaDB.Database
                                    .ExecuteSqlRawAsync("CALL CrearUsuarioMasivo ({0})",
                                        cm.IdcursoEva
                                   );
            }
            catch (Exception ex)
            {
                guardaSinError = false;
                if (ex.Message.Contains(Referencias.ErrorDuplicateEntry))
                {
                    await GuardarError(new LogErrorDTO(cm.IdSincronizacion)
                    {
                        TipoError = Referencias.ErrorDBMoodle,
                        Error = $"Ocurrió un error en Moodle al momento de crear usuarios masivos {(ex.Message.Contains(Referencias.ErrorDuplicateEntry) ? ", se detectan valores duplicados" : "")}"
                    });
                }

            }

            if (guardaSinError)
            {
                await _db.EstudianteMigrado.AddRangeAsync(em);
                await _db.SaveChangesAsync();

            }

        }

        private async Task GuardarEstudiantesNoValidos(List<VwestudiantesInscritosciclosactivos> estudiantes, int idSincronizacion)
        {
            List<LogError> estudiantesNoValidos = estudiantes
                                            .Where(w => !w.CorreoEstudiante.Contains("@") || !w.CorreoEstudiante.EndsWith(Referencias.upoliMailDomain))
                                            .Select(s => new LogError()
                                            {
                                                TipoError = Referencias.ErrorCorreoInvalido,
                                                Error = !s.CorreoEstudiante.Contains("@") ?
                                                                $"El estudiante {s.Nombre} no posee un correo válido" :
                                                                $"El correo {s.CorreoEstudiante} del estudiante {s.Nombre} no pertenece a una cuenta institucional de UPOLI",
                                                IdSincronizacion = idSincronizacion
                                            })
                                            .ToList();

            await _db.LogError.AddRangeAsync(estudiantesNoValidos);
            await _db.SaveChangesAsync();
        }

        private async Task<List<VwestudiantesInscritosciclosactivos>> ObtenerEstudiantesActivosCiclo(int cursoId)
        {
            return await _db.VwestudiantesInscritosciclosactivos
                                                .Where(w => w.CursoId == cursoId)
                                                .ToListAsync();
        }

        private async Task<InscribirEstudianteEVA> InscribirEstudianteEVA(VwestudiantesInscritosciclosactivos estudiante, long idCursoEva)
        {
            InscribirEstudianteEVA estudianteMigrado = (await _mariaDB.InscribirEstudianteEVA
                               .FromSqlRaw("CALL EnrollUserEVA ({0},{1},{2},{3},{4})",
                                   idCursoEva,
                                   estudiante.CorreoEstudiante,
                                   estudiante.Nombres,
                                   estudiante.Apellidos,
                                   estudiante.EstadoInscripcion
                               ).ToListAsync())
                               .FirstOrDefault();
            return estudianteMigrado;
        }

        private async Task AlmacenarEstudianteMigrado(CursoMigrado cm, VwestudiantesInscritosciclosactivos estudiante, int idSincronizacion)
        {
            EstudianteMigrado em = new EstudianteMigrado()
            {
                CursoId = cm.CursoId,
                Carnet = estudiante.Carnet,
                FechaCreacion = DateTime.Now,
                IdSincronizacion = idSincronizacion,
                Estado = estudiante.EstadoInscripcion
            };

            await _db.EstudianteMigrado.AddAsync(em);
            await _db.SaveChangesAsync();
        }

        private async Task<CursoMigrado> AlmacenarCursoMigrado(InscribirCursoEVA data, PlanificacionAcademicaDocenteDTO item, InfoEquivalencia tieneEquivalencia, Sincronizacion sincro)
        {
            CursoMigrado cm = new CursoMigrado();
            cm.IdcursoEva = data.IDCursoEva;
            cm.CursoId = item.planificaciones.CursoId;
            cm.Categoria = tieneEquivalencia.IdCategoriaEva;
            cm.FechaCreacion = DateTime.Now;
            cm.IdSincronizacion = sincro.IdSincronizacion;

            await _db.CursoMigrado.AddAsync(cm);
            await _db.SaveChangesAsync();

            return cm;
        }

        private async Task<InscribirCursoEVA> InscribirCursoEVA(PlanificacionAcademicaDocenteDTO item, long idCategoriaEva)
        {
            List<InscribirCursoEVA> data = await _mariaDB
                        .InscribirCursoEVA
                        .FromSqlRaw(
                            "CALL CreateCurso ({0},{1},{2},{3},{4},{5},{6})",
                            item.planificaciones.Asignatura,
                            item.planificaciones.NomeclaturaCurso,
                            item.planificaciones.CursoId,
                            idCategoriaEva,
                            item.docentes.Correo,
                            item.docentes.Nombre,
                            item.docentes.Apellidos
                        )
                        .ToListAsync();
            return data.FirstOrDefault();
        }

        private async Task<Sincronizacion> GuardarSincronizacion(SyncronizacionSaveDTO sincroSave, ServerInfo server)
        {
            Sincronizacion sincro = new Sincronizacion()
            {
                Fecha = server.Fecha,
                Tipo = sincroSave.TipoSincronizacion,
                EnProceso = true,
                FechaInicio = server.Fecha,
                FechaDesde = sincroSave.FechaDesde,
                IdUsuario = sincroSave.UserId
            };

            await _db.Sincronizacion.AddAsync(sincro);
            await _db.SaveChangesAsync();

            return sincro;
        }
        private async Task GuardarError(LogErrorDTO error)
        {
            LogError logError = Mapper<LogErrorDTO, LogError>.Map(error);
            await _db.LogError.AddAsync(logError);
            await _db.SaveChangesAsync();
        }

        private async Task<List<PlanificacionAcademicaDocenteDTO>> ObtenerCursosMigrar()
        {
            return await _db
            .VwplanificacionAcademica
            .Join(_db.VwperfilDocente,
                planificacion => planificacion.DocenteId,
                docente => docente.CodigoDocente,
                (planificaciones, docentes) => new PlanificacionAcademicaDocenteDTO
                {
                    planificaciones = planificaciones,
                    docentes = docentes
                }
            ).ToListAsync();
        }

        public async Task<Paginacion<SyncronizacionDTO>> ObtenerSincronizaciones(Paginado paginado)
        {
            PaginarExtension p = new PaginarExtension();
            IQueryable<SyncronizacionDTO> query = _db.Sincronizacion.Select(s => new SyncronizacionDTO
            {
                IdSincronizacion = s.IdSincronizacion,
                Fecha = s.Fecha,
                EstudiantesMigrados = s.EstudianteMigrado.Count(),
                CursosMigrados = s.CursoMigrado.Count(),
                Errores = s.LogError.Count(),
                Tipo = s.Tipo ? "Automática" : "Manual",
                Estado = s.EnProceso ? "En Progreso" : "Finalizado",
                Usuario = s.IdUsuarioNavigation.Usuario1,
                FechaDesde = s.FechaDesde,
                FechaFin = s.FechaFin
            });

            return await p.paginar(query, paginado);
        }

        public async Task<Paginacion<LogError>> ObtenerErroresSincronizacion(int id, Paginado paginado)
        {
            IQueryable<LogError> query = _db.LogError.Where(w => w.IdSincronizacion == id);
            if (await query.AnyAsync())
            {
                PaginarExtension p = new PaginarExtension();
                query = query.Select(s => new LogError
                {
                    TipoError = s.TipoError,
                    Error = s.Error,
                    FechaError = s.FechaError,
                });
                return await p.paginar<LogError>(query, paginado);
            }
            else
            {
                throw new NotFoundException();
            }

        }
    }
}