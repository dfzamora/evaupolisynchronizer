## Mapeo de datos SQL
dotnet ef dbcontext scaffold "Data Source=localhost;Initial Catalog=EnlaceEVA;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;" Microsoft.EntityFrameworkCore.SqlServer  -o Models/EnlaceEva -t VWPlanificacionAcademica -t VWPerfilEstudiantes -t VWPerfilDocente -t VWEstudiantesInscritosciclosactivos -t VWReporteMigrados -t VWReporteEstudianteNoMigrado -t CursoMigrado -t InfoEquivalencia -t LogError -t Sincronizacion -t VWInfoEquivalente -t VWCambiosDocentes -t EstudianteMigrado -t Usuario -c EnlaceEVAContext --context-dir Database -f

## paquete conexion Mysql
dotnet add package Pomelo.EntityFrameworkCore.MySql

## Mapeo de datos MariaDB
dotnet ef dbcontext scaffold "Server=107.23.22.81;Port=3306;Database=bitnami_moodle;User=jrtinoco;Password=jrtinoco2020" Pomelo.EntityFrameworkCore.MySql -o Models/bitnami_moodle -t mdl_course -t estudiantes -t vwcambiosdocentes -c MariaDBContext --context-dir Database -f