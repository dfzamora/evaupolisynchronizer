export const environment = {
  production: true, routes: {
    login: 'api/account/login',
    usuario: 'api/Usuario',
    infoenlace: 'api/InfoEnlace',
    sincronizacion: 'api/Sincronizacion',
    curso: 'api/Curso',
    reporte: 'api/Reporte',
  }
};
