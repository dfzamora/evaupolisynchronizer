import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CategoriasEVA } from '../Models/categorias-eva';
import { CategoriasUpoli } from '../Models/categorias-upoli';
import { InfoEnlaceApi } from '../Models/info-enlace';

@Injectable({
  providedIn: 'root'
})
export class InfoEnlaceService {

  href: string = environment.routes.infoenlace;

  constructor(private _httpClient: HttpClient) {

  }

  getData(sort: string, order: string, page: number, itemsPage: number): Observable<InfoEnlaceApi> {
    const requestUrl =
      `${this.href}?ordenarPor=${sort === undefined ? 'fechaCreacion' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<InfoEnlaceApi>(requestUrl);
  }

  getCategoriasEVA(): Observable<CategoriasEVA[]> {

    const requestUrl = `${this.href}/GetCategoriasEVA`;

    return this._httpClient.get<CategoriasEVA[]>(requestUrl);
  }

  getCategoriasUpoli(): Observable<CategoriasUpoli[]> {

    const requestUrl = `${this.href}/GetCategoriasUpoli`;

    return this._httpClient.get<CategoriasUpoli[]>(requestUrl);
  }

  DeleteEnlace(idInfoEnlace: number): Observable<{}> {
    return this._httpClient.delete(this.href + '/' + idInfoEnlace);
  }

  PostEnlace(ce: CategoriasEVA, cu: CategoriasUpoli): Observable<{}> {
    return this._httpClient.post(this.href + '/PostInfoEnlace', { categoryEva: ce, categoryUpoli: cu });
  }

}

