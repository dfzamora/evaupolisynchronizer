import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErroresApi } from '../Models/errores';
import { SincronizacionesApi } from '../Models/sincronizaciones';

@Injectable({
  providedIn: 'root'
})
export class SincronizacionesService {

  href: string = environment.routes.sincronizacion;


  constructor(private _httpClient: HttpClient) {
  }

  getData(sort: string, order: string, page: number, itemsPage: number): Observable<SincronizacionesApi> {
    const requestUrl =
      `${this.href}?ordenarPor=${sort === undefined ? 'IdSincronizacion' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<SincronizacionesApi>(requestUrl);
  }

  Sincronizar(fecha: Date): Observable<{}> {
    return this._httpClient.post(this.href,  fecha 
    );
  }

  getDataErrores(idSincronizacion: number, sort: string, order: string, page: number, itemsPage: number): Observable<ErroresApi> {
    const requestUrl =
      `${this.href}/${idSincronizacion}?ordenarPor=${sort === undefined ? 'fechaError' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ErroresApi>(requestUrl);
  }

}

