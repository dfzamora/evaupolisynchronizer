import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErroresApi } from '../Models/errores';
import { ReporteDetellaEstudiantesNoSincronizadosApi } from '../Models/reporte-detalle-estudiantes-no-sincronizados';
import { ReporteDocenteApi } from '../Models/reporte-docente-no-valido';
import { ReporteEstudianteApi } from '../Models/reporte-estudiante-no-valido';
import { ReporteSincronizadosApi } from '../Models/reporte-sincronizados';
import { SincronizacionesApi } from '../Models/sincronizaciones';
import { TotalEstudiantesMigrados } from '../Models/total-estudiantes-migrados';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  href: string = environment.routes.reporte;


  constructor(private _httpClient: HttpClient) {
  }

  getEstudiantesNoValidos(sort: string, order: string, page: number, itemsPage: number): Observable<ReporteEstudianteApi> {
    const requestUrl =
      `${this.href}/estudiantes-invalidos/?ordenarPor=${sort === undefined ? 'carnet' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ReporteEstudianteApi>(requestUrl);
  }

  getEstudiantesMigradosConsolidado(sort: string, order: string, page: number, itemsPage: number): Observable<ReporteSincronizadosApi> {
    const requestUrl =
      `${this.href}/estudiantes-sincronizados/?ordenarPor=${sort === undefined ? 'cursoId' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ReporteSincronizadosApi>(requestUrl);
  }

  getTotalEstudiantesMigrados(): Observable<TotalEstudiantesMigrados> {
    const requestUrl =
      `${this.href}/total-sincronizados`;

    return this._httpClient.get<TotalEstudiantesMigrados>(requestUrl);
  }

  getDetalleEstudiantesNoMigrados(ciclo: number, curso: number, sort: string, order: string, page: number, itemsPage: number): Observable<ReporteDetellaEstudiantesNoSincronizadosApi> {
    const requestUrl =
      `${this.href}/detalle-estudiantes-no-sincronizados/${ciclo}/${curso}/?ordenarPor=${sort === undefined ? 'cursoId' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ReporteDetellaEstudiantesNoSincronizadosApi>(requestUrl);
  }

  getErrores(sort: string, order: string, page: number, itemsPage: number): Observable<ErroresApi> {
    const requestUrl =
      `${this.href}/errores-servidor/?ordenarPor=${sort === undefined ? 'idlogError' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ErroresApi>(requestUrl);
  }

  getDocentesNoValidos(sort: string, order: string, page: number, itemsPage: number): Observable<ReporteDocenteApi> {
    const requestUrl =
      `${this.href}/docentes-invalidos/?ordenarPor=${sort === undefined ? 'codigoDocente' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<ReporteDocenteApi>(requestUrl);
  }




}

