import {
  Injectable
} from '@angular/core';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  
  constructor() {

  }

  exportexcel(data: any, headers: any, sheetName: string, fileName): void {
    let EXCEL_EXTENSION = '.xlsx';
    let worksheet: XLSX.WorkSheet;
    let customHeader = true;

    if (customHeader) {
      data.unshift(headers); // if custom header, then make sure first row of data is custom header 
      worksheet = XLSX.utils.json_to_sheet(data, { skipHeader: true });
    } else {
      worksheet = XLSX.utils.json_to_sheet(data);
    }
    const workbook = XLSX.utils.book_new();
    const fileNameComplete = fileName + new Date().toLocaleString() + EXCEL_EXTENSION;
    XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);
    XLSX.writeFile(workbook, fileNameComplete);


  }

}  