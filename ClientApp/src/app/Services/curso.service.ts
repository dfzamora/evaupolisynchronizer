import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Curso, CursoTest } from '../Models/curso';
import { ErroresApi } from '../Models/errores';

@Injectable({
  providedIn: 'root'
})
export class CursoService {
  href: string = environment.routes.curso;

  constructor(private _httpClient: HttpClient) {

  }

  PostCurso(curso: Curso): Observable<{}> {
    return this._httpClient.post(this.href, curso);
  }
  PostCursoTest(curso: CursoTest): Observable<{}> {
    return this._httpClient.post(this.href, curso);
  }
  ObtenerCursos(): Observable<CursoTest[]> {
    return this._httpClient.get<CursoTest[]>(this.href);
  }


}
