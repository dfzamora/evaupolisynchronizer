import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Account } from '../Models/account';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLogin = false;
  roleAs: string;

  constructor(private http: HttpClient) { }

  Login(cuenta: Account): Observable<{}> {
    return this.http.post<Account>(environment.routes.login, cuenta);
  }

  Logout() {
    sessionStorage.clear();
    window.location.href = '';

  }

  isLogged() {
    var user = sessionStorage.getItem('user');
    return !(user === null || user === undefined);
  }

  getRole() {
    this.roleAs = sessionStorage.getItem('rol');
    return this.roleAs;
  }

}
