import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Account } from '../Models/account';
import { UsuariosApi } from '../Models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  href: string = environment.routes.usuario;
 

  constructor(private _httpClient: HttpClient) {
    }

  getData(sort: string, order: string, page: number, itemsPage: number): Observable<UsuariosApi> {
    const requestUrl =
      `${this.href}?ordenarPor=${sort === undefined ? 'Usuario1' : sort}&orden=${order === '' ? 'desc' : order}&pagina=${page + 1}&registroPorPagina=${itemsPage}`;

    return this._httpClient.get<UsuariosApi>(requestUrl );
  }

  PostUsuario(Usuario: Account): Observable<{}> {
    return this._httpClient.post(this.href, Usuario 
    );
  }

  PutUsuario(Usuario: Account): Observable<{}> {
    return this._httpClient.put(this.href, Usuario 
    );
  }

  PatchUsuario(Usuario: Account): Observable<{}> {
    return this._httpClient.patch(this.href, Usuario 
    );
  }

  DeleteUsuario(id: number): Observable<{}> {
    return this._httpClient.delete(this.href + '/' + id 
    );
  }

}

