import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SincroTestComponent } from './Components/sincro-test/sincro-test.component';
import { EnlaceInfoComponent } from './Components/enlace-info/enlace-info.component';
import { SincronizacionesComponent } from './Components/sincronizaciones/sincronizaciones.component';
import { ErroresComponent } from './Components/errores/errores.component';
import { AuthGuard } from './Guard/auth-guard.service';
import { InicioComponent } from './Components/inicio/inicio.component';
import { LoginComponent } from './Components/login/login.component';
import { UsuarioComponent } from './Components/usuario/usuario.component';
import { ReporteComponent } from './Components/reporte/reporte.component';
import { EstudiantesSincronizadosComponent } from './Components/reporte/estudiantes-sincronizados/estudiantes-sincronizados.component';
import { ErroresServidorComponent } from './Components/reporte/errores-servidor/errores-servidor.component';
import { DetalleEstudiantesNoSincronizadosComponent } from './Components/reporte/detalle-estudiantes-no-sincronizados/detalle-estudiantes-no-sincronizados.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent, pathMatch: 'full' },
      
      { path: 'usuarios', component: UsuarioComponent,canActivate: [AuthGuard], data: { role: 'Administrador' }, pathMatch: 'full' },
      { path: 'sincro-test', component: SincroTestComponent, canActivate: [AuthGuard], data: { role: 'Administrador' }, pathMatch: 'full' },
      { path: 'enlace-info', component: EnlaceInfoComponent, canActivate: [AuthGuard], data: { role: 'Administrador' }, pathMatch: 'full' },
      { path: 'sincronizaciones', component: SincronizacionesComponent, canActivate: [AuthGuard], data: { role: 'Administrador' }, pathMatch: 'full' },
      { path: 'sincronizaciones/errores/:sincronizacion', component: ErroresComponent, canActivate: [AuthGuard], data: { role: 'Administrador' }, pathMatch: 'full' },
      
      { path: '', component: InicioComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'reporte/no-validos', component: ReporteComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'reporte/consolidado', component: EstudiantesSincronizadosComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'reporte/consolidado/no-sincronizados/:ciclo/:curso', component: DetalleEstudiantesNoSincronizadosComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'reporte/errores-servidor', component: ErroresServidorComponent, canActivate: [AuthGuard], pathMatch: 'full' },
    ]),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
