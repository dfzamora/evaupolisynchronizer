import { Component } from '@angular/core';
import { Curso } from 'src/app/Models/curso';
import { CursoService } from 'src/app/Services/curso.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  curso: Curso = new Curso();

  url_response: any;

  constructor(private cursoService: CursoService) {
  }

  crearcurso(): void {
    this.cursoService.PostCurso(this.curso)
      .subscribe(
        (data: any) => {
          this.url_response = data;
          this.limpiar();
        }
      );
  }

  limpiar(): void {
    this.curso.docente = "";
    this.curso.asignatura = "";
    this.curso.carrera = "";
    this.curso.genero = false;
  }
}
