import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Account } from 'src/app/Models/account';
import { AuthService } from 'src/app/Services/auth.service';
import { SnackService } from 'src/app/Services/snack.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  userControl = new FormControl('', Validators.required);
  passControl = new FormControl('', Validators.required);
  public flag: boolean = true;
  public cuenta: Account = { usuario1: "", pass: "", token: "", isAdmin: false };
  hide = true;
  constructor(private authService: AuthService, private router: Router, private snack: SnackService) { }

  ngOnInit(): void {
    this.redirectOnLogin();
  }

  redirectOnLogin() {
    var user = sessionStorage.getItem('user');

    if (user !== undefined || user !== null) {
      this.router.navigate(['/']);
    }
  }


  private validarCampos(): boolean {
    if (this.userControl.invalid) {
      this.userControl.markAsTouched();
      return false;
    } else if (this.passControl.invalid) {
      this.passControl.markAsTouched();
      return false;
    } else {
      return true;
    }
  }

  login(): void {

    if (this.validarCampos()) {
      this.flag = false;

      this.authService.Login(this.cuenta)
        .subscribe((data: Account) => {
          if (data.token !== undefined) {

            sessionStorage.setItem('user', data.token);
            sessionStorage.setItem('login', this.cuenta.usuario1);
            if (data.isAdmin) {
              sessionStorage.setItem('rol', "Administrador")
            }
            window.location.href = '';

          } else {
            this.snack.openSnackBar('No se ha podido iniciar sesión', '')
            this.flag = true;
          }


        }
          , error => {
            this.snack.openSnackBar(error.error.error, '');
            this.flag = true;


          }
        );

    }


  }

}

