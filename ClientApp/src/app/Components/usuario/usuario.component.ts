import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Account } from 'src/app/Models/account';
import { Usuarios } from 'src/app/Models/usuario';
import { ExcelService } from 'src/app/Services/excel.service';
import { SnackService } from 'src/app/Services/snack.service';
import { UsuarioService } from 'src/app/Services/usuario.service';
import { DialogConfirmComponent } from 'src/app/shared/dialog-confirm/dialog-confirm.component';
import { AddUsuarioDialog } from './add-usuario-dialog/add-usuario.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['edit', 'delete', 'idUsuario', 'usuario1', 'pass', 'administrador', 'fechaCreacion'];
  isLoadingResults = true;
  resultsLength = 0;
  data: Usuarios[] = [];
  dataSubscription: Subscription;
  public login: string = sessionStorage.getItem('login');

  public flag: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private usuarioService: UsuarioService, private excelExport: ExcelService, public dialog: MatDialog, private snackBar: SnackService, private router: Router) { }
  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }


  exportexcel(): void {
    const headers: any = { idUsuario: 'No.Usuario', usuario1: 'Usuario', isAdmin: "Administrador", fechaCreacion: 'Fecha de Creacion' };

    this.excelExport.exportexcel(
      this.data.map(m => <Usuarios>{ idUsuario: m.idUsuario, usuario1: m.usuario1, fechaCreacion: m.fechaCreacion, isAdmin: m.isAdmin }
      ), headers, 'Usuarios', 'Listado de Usuarios '
    );


  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.LoadTableFirstTime();
  }

  private CrearLlamadoServicio() {
    return merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.usuarioService!.getData(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;

          return observableOf([]);
        })
      );
  }

  private LoadTableFirstTime() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.Suscribirse(this.CrearLlamadoServicio());
  }

  Suscribirse(obser: Observable<any>) {
    this.dataSubscription = obser.subscribe(data => this.data = data);
  }


  Nuevo() {
    this.AbrirModal();
  }

  resetPass(usuario: Account) {
    this.AbrirModal(usuario);
  }

  editUser(usuario: Account) {
    this.AbrirModal(usuario, true);
  }

  // Mensaje de confirmacion para eliminar
  ConfirmaEliminar(dato: Usuarios): void {

    this.dialog
      .open(DialogConfirmComponent, {
        data: {
          titulo: `Confirmación de eliminación`,
          mensaje: `Si elimina al usuario, este no podrá volver a iniciar sesión en el sistema ¿Realmente desea continuar?`
        }
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) { //El usuario desea eliminar el registro
          this.EliminarUsuario(dato.idUsuario);

        }
      });


  }

  RefrescarSubscripcion() {
    this.dataSubscription.unsubscribe();
    this.Suscribirse(this.CrearLlamadoServicio())
  }

  // Accion de eliminar
  private EliminarUsuario(id: number) {
    this.flag = false;


    this.usuarioService.DeleteUsuario(id)
      .subscribe(
        (data: any) => {
          this.RefrescarSubscripcion();

          this.snackBar.openSnackBar('Se ha eliminado el usuario ', 'ENTENDIDO');
          this.flag = true;
        }, error => {
          if (error.status == 404) {
            this.snackBar.openSnackBar('El registro no ha sido encontrado', '');

          } else {

            this.snackBar.openSnackBar('Ocurrió un error inesperado', '');
          }
          this.flag = true;
        }
      )
  }

  private AbrirModal(usuario: Account = null, isEdit: boolean = false) {

    this.dialog.open(AddUsuarioDialog, {
      data: { usuario, isEdit },
      width: '800px'
    }).afterClosed()
      .subscribe((isAdding: Boolean) => {
        if (isAdding) { //El usuario desea agregó un nuevo registro
          this.RefrescarSubscripcion()
        }
      });;
  }

}

