import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Account } from 'src/app/Models/account';
import { SnackService } from 'src/app/Services/snack.service';
import { UsuarioService } from 'src/app/Services/usuario.service';
import { DialogConfirmComponent } from 'src/app/shared/dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'add-usuario-dialog',
  templateUrl: 'add-usuario-dialog.html'
})
export class AddUsuarioDialog implements OnInit {

  usuarioControl = new FormControl('', Validators.required);
  passControl = new FormControl('', Validators.required);
  repassControl = new FormControl('', Validators.required);
  isAdminControl = new FormControl(false);

  public flag: boolean = true;
  hide = true;

  usuario: Account;
  isEdit: boolean;

  public titulo: string = 'Nuevo Usuario';
  public texto_boton: string = 'Crear Usuario';

  constructor(@Inject(MAT_DIALOG_DATA) data: any, public dialogo: MatDialogRef<DialogConfirmComponent>, private snackBar: SnackService, private usuarioService: UsuarioService) {
    this.usuario = data.usuario;
    this.isEdit = data.isEdit;
  }


  ngOnInit(): void {
    if (this.usuario !== null) {
      this.inicializarRessetOrUpdate();
    }

  }

  inicializarRessetOrUpdate() {
    if (!this.isEdit) {
      this.usuarioControl.disable();
      this.isAdminControl.disable();
    }
    this.titulo = this.isEdit ? 'Editar Usuario' : 'Cambio de contraseña';
    this.texto_boton = this.isEdit ? 'Actualizar' : 'Cambiar contraseña';
    this.usuarioControl.setValue(this.usuario.usuario1);
    this.isAdminControl.setValue(this.usuario.isAdmin);
  }

  resetPass(pass: string, repass: string): void {
    if (this.validarCampos()) {
      if (this.validarPass(pass, repass)) {
        this.flag = false;
        this.usuario.pass = pass;
        this.usuarioService.PatchUsuario(this.usuario).subscribe(result => {
          this.snackBar.openSnackBar('Se ha cambiado la contraseña de manera exitosa', 'ENTENDIDO');
          this.flag = true;
          this.dialogo.close(true);
        }, error => {
          this.snackBar.openSnackBar('Ocurrió un error inesperado', '');
          this.flag = true;

        });
      } else {
        this.snackBar.openSnackBar('Las contraseñas no son iguales, debe escribir la misma contraseña en el campo repetir', '');

      }
    }
  }

  crearUsuario(usuario: string, pass: string, repass: string, isAdmin: boolean): void {
    if (this.validarCampos()) {
      if (this.validarPass(pass, repass)) {
        this.flag = false;
        this.usuarioService.PostUsuario({ usuario1: usuario, pass: pass, token: null, isAdmin: isAdmin }).subscribe(result => {
          this.snackBar.openSnackBar('Se ha creado el usuario de manera exitosa', 'ENTENDIDO');
          this.flag = true;
          this.dialogo.close(true);
        }, error => {
          this.snackBar.openSnackBar(error.error.error, '');
          this.flag = true;

        });
      } else {
        this.snackBar.openSnackBar('Las contraseñas no son iguales, debe escribir la misma contraseña en el campo repetir', '');

      }
    }

  }

  editUser(usuario: string, isAdmin: boolean): void {
    if (this.usuarioControl.invalid) {
      this.usuarioControl.markAsTouched();
    } else {
      this.flag = false;
      this.usuario.usuario1 = usuario;
      this.usuario.isAdmin = isAdmin;
      this.usuarioService.PutUsuario(this.usuario).subscribe(result => {
        this.snackBar.openSnackBar('Se ha editado el usuario de manera exitosa', 'ENTENDIDO');
        this.flag = true;
        this.dialogo.close(true);
      }, error => {
        this.snackBar.openSnackBar(error.error.error, '');
        this.flag = true;

      });
    }
  }


  validarPass(pass: string, repass: string) {
    return pass === repass;
  }

  private validarCampos(): boolean {
    if (this.usuarioControl.invalid) {
      this.usuarioControl.markAsTouched();
      return false;
    } else if (this.passControl.invalid) {
      this.passControl.markAsTouched();
      return false;
    } else if (this.repassControl.invalid) {
      this.repassControl.markAsTouched();
      return false;
    } else {
      return true;
    }
  }

  cancelar(): void {
    this.dialogo.close();
  }

}
