import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { CategoriasEVA } from 'src/app/Models/categorias-eva';
import { CategoriasUpoli } from 'src/app/Models/categorias-upoli';
import { InfoEnlaceService } from 'src/app/Services/info-enlace.service';
import { InfoEnlace } from '../../Models/info-enlace';
import { ExcelService } from '../../Services/excel.service';
import { SnackService } from '../../Services/snack.service';
import { DialogConfirmComponent } from '../../shared/dialog-confirm/dialog-confirm.component';
import { AddEnlaceDialog } from './add-enlace-dialog/add-enlace-dialog.component';

/**
 * @title Table retrieving data through HTTP
 */
@Component({
  selector: 'app-enlace-info',
  styleUrls: ['./enlace-info.component.scss'],
  templateUrl: './enlace-info.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EnlaceInfoComponent implements AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['ramaUpoli', 'ramaEva', 'fechaCreacion'];

  data: InfoEnlace[] = [];
  dataSubscription: Subscription;
  expandedElement: InfoEnlace | null;
  public flag: boolean = true;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  public categoriaEva: CategoriasEVA[];
  categoriaUpoli: CategoriasUpoli[];



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private infoEnlaceService: InfoEnlaceService, 
    private excelExport: ExcelService, 
    public dialog: MatDialog, 
    private snack: SnackService) {

  }

  exportexcel(): void {
    const headers: any = { ramaUpoli: 'Rama UPOLI', ramaEva: 'Rama EVA', fechaCreacion: 'Fecha de Creación' };

    var dataFilter: any = this.data.map(item => ({ ramaEva: item.ramaEva, ramaUpoli: item.ramaUpoli, fechaCreacion: item.fechaCreacion }));

    this.excelExport.exportexcel(
      dataFilter, headers, 'Enlaces', 'Listado de enlaces entre UPOLI y EVA'
    );

  }

  ngAfterViewInit() {

    this.LoadTableFirstTime();
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }


  private CrearLlamadoServicio() {

    // If the user changes the sort order, reset back to the first page.

    return merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.infoEnlaceService!.getData(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the  API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      );


  }

  LoadTableFirstTime() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.Suscribirse(this.CrearLlamadoServicio());
  }

  Suscribirse(obser: Observable<any>) {
    this.dataSubscription = obser.subscribe(data => this.data = data);
  }

  RefrescarSubscripcion() {
    this.dataSubscription.unsubscribe();
    this.Suscribirse(this.CrearLlamadoServicio())
  }

  // Mensaje de confirmacion para eliminar
  ConfirmaEliminar(dato: InfoEnlace): void {

    this.dialog
      .open(DialogConfirmComponent, {
        data: {
          titulo: `Confirmación de eliminación`,
          mensaje: `Si elimina este enlace, no será posible seguir sincronizando la carrera '` + dato.carrera + `' de la escuela '` + dato.escuela + `' en el recinto '` + dato.recinto + `'  ¿Realmente desea continuar?`
        }
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) { //El usuario desea eliminar el registro
          this.EliminarEnlace(dato.idInfoEquivalencia);

        }
      });


  }

  // Accion de eliminar
  private EliminarEnlace(id: number) {
    this.flag = false;


    this.infoEnlaceService.DeleteEnlace(id)
      .subscribe(
        (data: any) => {
          this.RefrescarSubscripcion();

          this.snack.openSnackBar('Se ha eliminado el enlace entre Upoli y Eva ', 'ENTENDIDO');
          this.flag = true;
        }, error => {
          if (error.status == 404) {
            this.snack.openSnackBar('El registro no ha sido encontrado', '');

          } else {

            this.snack.openSnackBar('Ocurrió un error inesperado', '');
          }
          this.flag = true;
        }
      )
  }

  Nuevo() {
    this.AbrirModal();
  }

  private AbrirModal() {
    this.dialog.open(AddEnlaceDialog, {
      width: '800px'
    }).afterClosed()
      .subscribe((isAdding: Boolean) => {
        if (isAdding) { //El usuario desea agregó un nuevo registro
          this.RefrescarSubscripcion();
        }
      });
  }


}



