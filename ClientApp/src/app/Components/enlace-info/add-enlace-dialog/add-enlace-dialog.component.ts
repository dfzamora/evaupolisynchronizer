import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CategoriasEVA } from "src/app/Models/categorias-eva";
import { CategoriasUpoli } from "src/app/Models/categorias-upoli";
import { InfoEnlaceService } from "src/app/Services/info-enlace.service";
import { DialogConfirmComponent } from "src/app/shared/dialog-confirm/dialog-confirm.component";
import { SnackBarShared } from "src/app/shared/SnackBar/snack";

@Component({
  selector: 'add-enlace-dialog',
  templateUrl: 'add-enlace-dialog.html'
})
export class AddEnlaceDialog implements OnInit {

  public categoriaEva: CategoriasEVA[] = [];
  public categoriaUpoli: CategoriasUpoli[] = [];
  private snackBar = new SnackBarShared();

  categoriaEvaControl = new FormControl('', Validators.required);
  categoriaUpoliControl = new FormControl('', Validators.required);

  public flag: boolean = true;

  constructor(
    private infoEnlaceService: InfoEnlaceService,
    public dialogo: MatDialogRef<DialogConfirmComponent>,
    private snack: MatSnackBar
  ) {


  }

  ngOnInit(): void {

    this.infoEnlaceService.getCategoriasEVA().subscribe(result => {
      this.categoriaEva = result;
    }, error => {
      this.snackBar.openSnackBar(this.snack, 'No se ha podido cargar la información de categorias EVA ', '');
    });

    this.infoEnlaceService.getCategoriasUpoli().subscribe(result => {
      this.categoriaUpoli = result;

    }, error => {
      this.snackBar.openSnackBar(this.snack, 'No se ha podido cargar la información de categorias UPOLI ', '');
    });
  }

  guardarEnlace(ce: CategoriasEVA, cu: CategoriasUpoli): void {
    if (this.validarCampos()) {

      this.flag = false;
      this.infoEnlaceService.PostEnlace(ce, cu).subscribe(result => {

        this.snackBar.openSnackBar(this.snack, 'Se ha creado el enlace, su sincronización será realizada en el próximo proceso ', 'ENTENDIDO');
        this.flag = true;
        this.dialogo.close(true);
      }, error => {
        this.snackBar.openSnackBar(this.snack, 'Ocurrió un error inesperado', '');
        this.flag = true;
      }
      );
    }
  }

  private validarCampos(): boolean {
    if (this.categoriaEvaControl.invalid) {
      this.categoriaEvaControl.markAsTouched();
      return false;
    } else if (this.categoriaUpoliControl.invalid) {
      this.categoriaUpoliControl.markAsTouched();
      return false;
    } else {
      return true;
    }
  }

  cancelar(): void {
    this.dialogo.close();
  }

}


