import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { SincronizacionesService } from 'src/app/Services/sincronizaciones.service';
import { Errores } from '../../Models/errores';
import { ExcelService } from '../../Services/excel.service';

@Component({
  selector: 'app-errores',
  templateUrl: './errores.component.html',
  styleUrls: ['./errores.component.scss']
})
export class ErroresComponent implements AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['tipoError', 'error', 'fechaError'];
  isLoadingResults = true;
  resultsLength = 0;
  data: Errores[] = [];
  public flag: boolean = true;
  idSincronizacion: number = 0;
  dataSubscription: Subscription;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private sincronizacionesService: SincronizacionesService, private excelExport: ExcelService, private route: ActivatedRoute) {

    this.route.params.subscribe(routeParams => {
      this.idSincronizacion = routeParams.sincronizacion;
    });
  }

  exportexcel(): void {
    const headers: any = {
      tipoError: 'Tipo',
      error: 'Error',
      fechaError: 'Fecha'
    };

    this.excelExport.exportexcel(
      this.data, headers, 'Errores de sincronizacion', 'Errores de la Sincronización No.' + this.idSincronizacion + ' '
    );


  }


  ngAfterViewInit() {
    this.LoadTable();
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }


  private LoadTable() {

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.dataSubscription = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.sincronizacionesService!.getDataErrores(
            this.idSincronizacion,
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;

          return observableOf([]);
        })
      ).subscribe(data => { this.data = data; });

  }



}

