import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CursoTest } from 'src/app/Models/curso';
import { CursoService } from 'src/app/Services/curso.service';
import { SnackService } from '../../Services/snack.service';




@Component({
  selector: 'app-sincro-test',
  templateUrl: './sincro-test.component.html',
  styleUrls: ['./sincro-test.component.scss']
})
export class SincroTestComponent implements OnInit {

  cursoControl = new FormControl('', Validators.required);
  selectFormControl = new FormControl('', Validators.required);
  cursos: CursoTest[];
  public flag: boolean = true;

  constructor(private cursoService:CursoService, private snack: SnackService) {
  }
  ngOnInit(): void {
    this.obtenerCursos();
  }

  obtenerCursos(): void {
    this.cursoService.ObtenerCursos().subscribe(result => {
      this.cursos = result;
    }, error => console.error(error));
  }

  registrarCurso(c: CursoTest): void {
    if (this.validarCampos()) {

      this.flag = false;
      this.cursoService.PostCursoTest(c).subscribe(result => {

        this.limpiarSeleccion();

        this.snack.openSnackBar('Se ha creado el curso ' + c.nomeclaturaCurso, 'ENTENDIDO');
        this.flag = true;
      }, error => {
        this.snack.openSnackBar('Ocurrió un error inesperado', '');
        this.flag = true;
      }
      );
    }
  }
  validarCampos(): boolean {
    if (this.cursoControl.invalid) {
      this.cursoControl.markAsTouched();
      return false;
    } else {
      return true;
    }
  }

  limpiarSeleccion() {
    this.cursoControl.reset();
  }


}

