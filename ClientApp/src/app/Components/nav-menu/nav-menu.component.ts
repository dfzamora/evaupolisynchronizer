import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, HostBinding } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class NavMenuComponent {
  expanded: boolean = true;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  public user: string = sessionStorage.getItem('user');
  public login: string = sessionStorage.getItem('login');
  public rol: string = sessionStorage.getItem('rol');
  public isAdmin: boolean;
  anio: number;


  constructor(
    private authService: AuthService
  ) {
    this.anio = new Date().getFullYear()
    this.isAdmin= this.rol === 'Administrador'
  }

  logout() {
    this.authService.Logout()
  }

}
