import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/Models/format-datepicker';
import { DialogConfirmComponent } from 'src/app/shared/dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-sincro-desde-dialog',
  templateUrl: './sincro-desde-dialog.component.html',
  styleUrls: ['./sincro-desde-dialog.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class SincroDesdeDialogComponent implements OnInit {

  maxDate = new Date();
  sincroDesdeControl = new FormControl('', Validators.required)

  constructor(
    public dialogo: MatDialogRef<DialogConfirmComponent>,

  ) { }

  ngOnInit() {
  }

  cerrar(accion: boolean): void {
    if (accion) {
      this.dialogo.close(this.sincroDesdeControl.value);

    } else {
      this.dialogo.close(accion);

    }
  }
}
