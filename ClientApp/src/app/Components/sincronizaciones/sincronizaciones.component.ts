import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { SincronizacionesService } from 'src/app/Services/sincronizaciones.service';
import { Sincronizaciones } from '../../Models/sincronizaciones';
import { ExcelService } from '../../Services/excel.service';
import { SnackService } from '../../Services/snack.service';
import { SincroDesdeDialogComponent } from './sincro-desde-dialog/sincro-desde-dialog.component';

@Component({
  selector: 'app-sincronizaciones',
  templateUrl: './sincronizaciones.component.html',
  styleUrls: ['./sincronizaciones.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class SincronizacionesComponent implements AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['idSincronizacion', 'tipo', 'fecha','fechaFin', 'usuario', 'estado'];
  isLoadingResults = true;
  resultsLength = 0;
  data: Sincronizaciones[];
  dataSubscription = new Subscription();

  public flag: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private sincronizacionesService: SincronizacionesService,
    private excelExport: ExcelService,
    private snackBar: SnackService,
    public dialog: MatDialog,
  ) {

  }
  headers: any = {
    idSincronizacion: 'No.Sincronizacion',
    fecha: 'Fecha',
    fechaFin: 'Fecha Fin',
    estudiantesMigrados: 'Estudiantes Migrados',
    cursosMigrados: 'Cursos Migrados',
    errores: 'Errores',
    tipo: 'Tipo de Sincronizacion',
    usuario: "Generador",
    estado: "Estado"
  };

  exportexcel(): void {

    this.excelExport.exportexcel(
      this.data, this.headers, 'Sincronizaciones', 'Listado de Sincronizaciones '
    );


  }

  ngAfterViewInit() {
    this.LoadTableFirstTime();
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }


  // Carga de la informacion desde la BD
  private CrearLlamadoServicio() {

    return merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.sincronizacionesService!.getData(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      );
  }


  private LoadTableFirstTime() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.Suscribirse(this.CrearLlamadoServicio());
  }

  Suscribirse(obser: Observable<any>) {
    this.dataSubscription = obser.subscribe(data => this.data = data);
  }


  SincronizarDatos(fecha: Date=null) {
    this.flag = false;

    this.sincronizacionesService.Sincronizar(fecha)
      .subscribe(
        () => {
          this.RefrescarSubscripcion();
          this.snackBar.openSnackBar('Se han sincronizado los datos desde UPOLI hasta EVA', 'ENTENDIDO');
          this.flag = true;
        }, (error) => {
          this.snackBar.openSnackBar(error.error.mensaje, '');

          this.RefrescarSubscripcion();
          this.flag = true;
        }
      )
  }

  SincronizarDatosDesde() {
    this.dialog.open(SincroDesdeDialogComponent, {
      width: '800px'
    }).afterClosed()
      .subscribe((dialogResult: any) => {
        if (typeof dialogResult === 'object') {
          this.SincronizarDatos(dialogResult);
        }
      });
  }

  RefrescarSubscripcion() {
    this.dataSubscription.unsubscribe();
    this.Suscribirse(this.CrearLlamadoServicio())
  }


}
