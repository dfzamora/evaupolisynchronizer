import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ReporteSincronizados } from 'src/app/Models/reporte-sincronizados';
import { TotalEstudiantesMigrados } from 'src/app/Models/total-estudiantes-migrados';
import { ExcelService } from 'src/app/Services/excel.service';
import { ReporteService } from 'src/app/Services/reportes.service';

@Component({
  selector: 'app-estudiantes-sincronizados',
  templateUrl: './estudiantes-sincronizados.component.html',
  styleUrls: ['./estudiantes-sincronizados.component.scss']
})
export class EstudiantesSincronizadosComponent implements AfterViewInit {


  displayedColumns: string[] = ['see', 'cicloId', 'cursoId', 'nomeclaturaCurso', 'inscritos', 'migrados'];
  isLoadingResults = true;
  resultsLength = 0;
  data: ReporteSincronizados[];
  totalMigrados: Observable<TotalEstudiantesMigrados>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSubscription = new Subscription();

  constructor(
    private reporteService: ReporteService,
    private excelExport: ExcelService,
  ) { }

  headers: any = {
    cicloId: 'Cod.Ciclo',
    cursoId: 'Cod.Curso',
    nomeclaturaCurso: 'Nomensclatura del Curso',
    inscritos: 'Inscritos UOnline',
    migrados: 'Migrados EVA',
  };

  exportexcel(): void {

    this.excelExport.exportexcel(
      this.data, this.headers, 'Registros Sincronizados', 'Consolidado de Cursos Migrados '
    );


  }

  ngAfterViewInit() {
    this.LoadTableFirstTime();
    this.ObtenerTotalMigrados();
  }

  ObtenerTotalMigrados(): void {
    this.totalMigrados = this.reporteService.getTotalEstudiantesMigrados();
  }



  private LoadTableFirstTime() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.Suscribirse(this.CrearLlamadoServicio());
  }
  CrearLlamadoServicio(): Observable<any> {
    return merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.reporteService!.getEstudiantesMigradosConsolidado(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      );
  }


  Suscribirse(obser: Observable<any>) {
    this.dataSubscription = obser.subscribe(data => this.data = data);
  }
}
