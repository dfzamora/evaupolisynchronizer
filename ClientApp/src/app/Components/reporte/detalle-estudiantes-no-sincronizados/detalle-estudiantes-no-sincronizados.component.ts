import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { ReporteDetellaEstudiantesNoSincronizados } from 'src/app/Models/reporte-detalle-estudiantes-no-sincronizados';
import { ExcelService } from 'src/app/Services/excel.service';
import { ReporteService } from 'src/app/Services/reportes.service';
import { SincronizacionesService } from 'src/app/Services/sincronizaciones.service';

@Component({
  selector: 'app-detalle-estudiantes-no-sincronizados',
  templateUrl: './detalle-estudiantes-no-sincronizados.component.html',
  styleUrls: ['./detalle-estudiantes-no-sincronizados.component.scss']
})
export class DetalleEstudiantesNoSincronizadosComponent implements AfterViewInit, OnDestroy {


  displayedColumns: string[] = ['carnet', 'nombre', 'correoEstudiante', 'fechaInscripcion'];
  isLoadingResults = true;
  resultsLength = 0;
  data: ReporteDetellaEstudiantesNoSincronizados[] = [];
  public flag: boolean = true;
  idCurso: number = 0;
  idCiclo: number = 0;
  dataSubscription: Subscription;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private reporteService: ReporteService,
    private excelExport: ExcelService,
    private route: ActivatedRoute
  ) {

    this.route.params.subscribe(routeParams => {
      this.idCurso = routeParams.curso;
      this.idCiclo = routeParams.ciclo;
    });
  }

  exportexcel(): void {
    const headers: any = {
      carnet: 'Carnet',
      nombre: 'Estudiante',
      correoEstudiante: 'Correo Estudiante',
      fechaInscripcion: 'Fecha Inscripcion'
    };

    this.excelExport.exportexcel(
      this.data, headers, 'Estudiantes No Sincronizados', 'Estudiantes no Sincronizados del curso ' + this.idCurso
    );


  }


  ngAfterViewInit() {
    this.LoadTable();
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }


  private LoadTable() {

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.dataSubscription = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.reporteService!.getDetalleEstudiantesNoMigrados(
            this.idCiclo,
            this.idCurso,
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;

          return observableOf([]);
        })
      ).subscribe(data => { this.data = data; });

  }


}
