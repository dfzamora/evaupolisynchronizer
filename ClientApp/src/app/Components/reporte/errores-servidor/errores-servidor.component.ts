import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Errores, ErroresApi } from 'src/app/Models/errores';
import { ReporteSincronizados } from 'src/app/Models/reporte-sincronizados';
import { ExcelService } from 'src/app/Services/excel.service';
import { ReporteService } from 'src/app/Services/reportes.service';

@Component({
  selector: 'app-errores-servidor',
  templateUrl: './errores-servidor.component.html',
  styleUrls: ['./errores-servidor.component.scss']
})
export class ErroresServidorComponent implements AfterViewInit {



  displayedColumns: string[] = ['tipoError', 'error','idSincronizacion', 'fechaError'];
  isLoadingResults = true;
  resultsLength = 0;
  data: Errores[];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSubscription = new Subscription();

  constructor(
    private reporteService: ReporteService,
    private excelExport: ExcelService,
  ) { }

  headers: any = {
    tipoError: 'Tipo',
    error: 'Error',
    idSincronizacion:'No.Sincronizacion',
    fechaError: 'Fecha'
  };

  exportexcel(): void {

    this.excelExport.exportexcel(
      this.data, this.headers, 'Errores de Servidor', 'Errores de servidor de las sincronizaciones'
    );


  }

  ngAfterViewInit() {
    this.LoadTableFirstTime();
  }



  private LoadTableFirstTime() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.Suscribirse(this.CrearLlamadoServicio());
  }
  CrearLlamadoServicio(): Observable<any> {
    return merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.reporteService!.getErrores(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;

          return data.items;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      );
  }


  Suscribirse(obser: Observable<any>) {
    this.dataSubscription = obser.subscribe(data => this.data = data);
  }
}
