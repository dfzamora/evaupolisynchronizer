import { MatSnackBar } from "@angular/material/snack-bar";

export class SnackBarShared {


    openSnackBar(_snackBar: MatSnackBar, message: string, action: string) {
        _snackBar.open(message, action, {
            duration: 5000,
        });
    }
}