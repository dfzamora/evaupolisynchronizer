import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExcelService } from '../app/Services/excel.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddEnlaceDialog } from './Components/enlace-info/add-enlace-dialog/add-enlace-dialog.component';
import { EnlaceInfoComponent } from './Components/enlace-info/enlace-info.component';
import { ErroresComponent } from './Components/errores/errores.component';
import { HomeComponent } from './Components/home/home.component';
import { InicioComponent } from './Components/inicio/inicio.component';
import { LoginComponent } from './Components/login/login.component';
import { NavMenuComponent } from './Components/nav-menu/nav-menu.component';
import { DetalleEstudiantesNoSincronizadosComponent } from './Components/reporte/detalle-estudiantes-no-sincronizados/detalle-estudiantes-no-sincronizados.component';
import { DocentesNoValidosComponent } from './Components/reporte/docentes-no-validos/docentes-no-validos.component';
import { ErroresServidorComponent } from './Components/reporte/errores-servidor/errores-servidor.component';
import { EstudiantesNoValidosComponent } from './Components/reporte/estudiantes-no.validos/estudiantes-no-validos.component';
import { EstudiantesSincronizadosComponent } from './Components/reporte/estudiantes-sincronizados/estudiantes-sincronizados.component';
import { ReporteComponent } from './Components/reporte/reporte.component';
import { SincroTestComponent } from './Components/sincro-test/sincro-test.component';
import { SincroDesdeDialogComponent } from './Components/sincronizaciones/sincro-desde-dialog/sincro-desde-dialog.component';
import { SincronizacionesComponent } from './Components/sincronizaciones/sincronizaciones.component';
import { AddUsuarioDialog } from './Components/usuario/add-usuario-dialog/add-usuario.component';
import { UsuarioComponent } from './Components/usuario/usuario.component';
import { AuthGuard } from './Guard/auth-guard.service';
import { getSpanishPaginatorIntl } from './lang/spanish-paginator-intl';
import { MaterialModule } from './material-module';
import { AuthInterceptorService } from './Services/auth-interceptor.service';
import { CursoService } from './Services/curso.service';
import { InfoEnlaceService } from './Services/info-enlace.service';
import { AuthService } from './Services/auth.service';
import { ReporteService } from './Services/reportes.service';
import { SincronizacionesService } from './Services/sincronizaciones.service';
import { SnackService } from './Services/snack.service';
import { UsuarioService } from './Services/usuario.service';
import { DialogConfirmComponent } from './shared/dialog-confirm/dialog-confirm.component';
import { SharedModule } from './shared/shared.module';

const components = [
  AppComponent,
  NavMenuComponent,
  HomeComponent,
  SincroTestComponent,
  EnlaceInfoComponent,
  SincronizacionesComponent,
  ErroresComponent,
  AddEnlaceDialog,
  AddUsuarioDialog,
  SincroDesdeDialogComponent,
  LoginComponent,
  InicioComponent,
  UsuarioComponent,
  ReporteComponent,
  EstudiantesNoValidosComponent,
  DocentesNoValidosComponent,
  EstudiantesSincronizadosComponent,
  ErroresServidorComponent,
  DetalleEstudiantesNoSincronizadosComponent
]

const modules = [
  HttpClientModule,
  FormsModule,
  ReactiveFormsModule,
  BrowserModule,
  BrowserAnimationsModule,
  CommonModule,
  AppRoutingModule,
  MaterialModule,
  SharedModule
]

const services = [
  ExcelService,
  SnackService,
  AuthService,
  UsuarioService,
  InfoEnlaceService,
  SincronizacionesService,
  CursoService,
  ReporteService
];

@NgModule({
  declarations: [
    ...components
  ],
  entryComponents: [EnlaceInfoComponent, DialogConfirmComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    ...modules
  ],
  providers: [...services,
  [AuthGuard],
  { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
  { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
