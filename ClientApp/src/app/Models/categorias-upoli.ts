
export interface CategoriasUpoli {
  carrera: string;
  carreraId: number;
  escuela: string;
  escuelaId: string;
  rama: string;
  recinto: string;
  recintoId: number;
}
