
export interface UsuariosApi {
  items: Usuarios[];
  total_count: number;
}


export interface Usuarios {
  idUsuario: number;
  usuario1: string;
  fechaCreacion: Date;
  isAdmin:boolean;
}
