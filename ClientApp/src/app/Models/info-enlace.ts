export interface InfoEnlaceApi {
  items: InfoEnlace[];
  total_count: number;
}

export interface InfoEnlace {
  carrera: string;
  carreraId: number;
  escuela: string;
  escuelaId: number;
  idCategoriaEva: number;
  idInfoEquivalencia: number;
  idPadreEva: number;
  nameEva: string;
  ramaEva: string;
  ramaUpoli: string;
  recinto: string;
  recintoId: number;
  fechaCreacion: Date;
}

