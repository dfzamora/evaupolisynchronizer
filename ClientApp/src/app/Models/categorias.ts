import { CategoriasEVA } from './categorias-eva';
import { CategoriasUpoli } from './categorias-upoli';


export interface Categorias {
  caregoriaEVA: CategoriasEVA[];
  categoriaUPOLI: CategoriasUpoli[];
}
