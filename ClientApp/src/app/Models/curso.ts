export class Curso {
    asignatura: string;
    docente: string;
    carrera: string;
    genero: boolean;
}

export interface CursoTest {
    nomeclaturaCurso: string;
    asignatura: string;
    cursoId: number;
  }