
export interface ReporteSincronizadosApi {
  items: ReporteSincronizados[];
  total_count: number;
}


export interface ReporteSincronizados {
  cicloId: Number;
  cursoId: Number;
  nomeclaturaCurso: string;
  inscritos: Number;
  migrados: Number;
}
