
export interface ReporteEstudianteApi {
  items: Estudiante[];
  total_count: number;
}


export interface Estudiante {
  carnet: string;
  nombre: string;
  correoEstudiante: string;
}
