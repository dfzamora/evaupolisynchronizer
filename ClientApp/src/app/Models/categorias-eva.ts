
export interface CategoriasEVA {
  idEVA: number;
  nameEVA: string;
  parentEVA: number;
  ramaEVA: number;
}
