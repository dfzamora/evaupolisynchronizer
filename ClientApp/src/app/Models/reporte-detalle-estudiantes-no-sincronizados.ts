
export interface ReporteDetellaEstudiantesNoSincronizadosApi {
  items: ReporteDetellaEstudiantesNoSincronizados[];
  total_count: number;
}


export interface ReporteDetellaEstudiantesNoSincronizados {
  cicloId: number;
  cursoId: number;
  nomeclaturaCurso: string;
  carnet: string;
  nombre: string;
  correoEstudiante: string;
  fechaInscripcion: Date;
  fechaEstado: Date;
}
