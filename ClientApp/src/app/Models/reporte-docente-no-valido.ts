
export interface ReporteDocenteApi {
  items: Docente[];
  total_count: number;
}


export interface Docente {
  codigoDocente: string;
  nombre: string;
  correoDocente: string;
}
