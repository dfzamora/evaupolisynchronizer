
export interface SincronizacionesApi {
  items: Sincronizaciones[];
  total_count: number;
}


export interface Sincronizaciones {
  idSincronizacion: number;
  fecha: Date;
  fechaFin: Date;
  fechaDesde: Date;
  estudiantesMigrados: number;
  cursosMigrados: number;
  errores: number;
  tipo: string;
  estado: string;
  usuario: string;
}
