export interface ErroresApi {
  items: Errores[];
  total_count: number;
}


export interface Errores {
  error: string;
  fechaError: Date;
  tipoError: string;
}
