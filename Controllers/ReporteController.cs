using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moodle.Database;
using moodle.Models.EnlaceEva;
using Microsoft.AspNetCore.Http;
using moodle.Models.bitnami_moodle;
using System.Linq.Dynamic.Core;
using Models;
using Microsoft.AspNetCore.Authorization;
using services;
using System.Threading.Tasks;
using Util;

namespace moodle.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ReporteController : ControllerBase
    {
        private readonly IReporteService _reporteService;

        public ReporteController(IReporteService reporteService)
        {
            _reporteService = reporteService;
        }

        [HttpGet("total-sincronizados")]
        public async Task<IActionResult> GetTotalSincronizados()
        {
            try
            {
                ConsolidadoEstudiantesDTO data = await _reporteService.ObtenerTotalEstudiantesMigrados();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("estudiantes-sincronizados")]
        public async Task<IActionResult> GetEstudiantesSincronizados([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<VwreporteMigrados> data = await _reporteService.ObtenerEstudiantesMigrados(paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("detalle-estudiantes-no-sincronizados/{ciclo:int}/{curso:int}")]
        public async Task<IActionResult> GetDetalleEstudiantesNoSincronizados(int ciclo, int curso, [FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<VwreporteEstudianteNoMigrado> data = await _reporteService.ObtenerDetalleEstudiantesNoMigrados(ciclo, curso, paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("estudiantes-invalidos")]
        public async Task<IActionResult> GetEstudiantesNovalidos([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<EstudiantesDTO> data = await _reporteService.ObtenerEstudiantesNoValidos(paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("docentes-invalidos")]
        public async Task<IActionResult> GetDocentesNovalidos([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<DocentesDTO> data = await _reporteService.ObtenerDocentesNoValidos(paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("errores-servidor")]
        public async Task<IActionResult> GetErroresServidor([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<LogError> data = await _reporteService.ObtenerErroresServidor(paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }


    }
}
