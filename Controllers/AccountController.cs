using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moodle.Models.EnlaceEva;
using services;
using System.Threading.Tasks;

namespace moodle.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private IAuthenticateService _authenticationService;

        public AccountController( IAuthenticateService _authenticationService)
        {
            this._authenticationService = _authenticationService;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> login([FromBody] Usuario usuario)
        {
            var usr = await _authenticationService.Authenticate(usuario);

            if (usr.Usuario1 == null)
                return Unauthorized(new { error = "Contraseña incorrecta" });
            if (usr.Pass == null)
                return Unauthorized(new { error = "El usuario no existe" });

            return Ok(usr);



        }

    }

}
