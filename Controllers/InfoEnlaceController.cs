using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moodle.Database;
using moodle.Models.EnlaceEva;
using Microsoft.AspNetCore.Http;
using System.Linq.Dynamic.Core;
using Models;
using moodle.Models.bitnami_moodle;
using Microsoft.AspNetCore.Authorization;
using services;
using System.Threading.Tasks;
using Util;

namespace moodle.Controllers
{
   [Authorize(Policy = "RequireAdministratorRole")]
    [ApiController]
    [Route("api/[controller]")]
    public class InfoEnlaceController : ControllerBase
    {
        private readonly ICursoService _cursoService;
        private readonly IInfoEnlaceService _infoEnlaceService;
        public InfoEnlaceController(ICursoService cursoService, IInfoEnlaceService infoEnlaceService)
        {
            _cursoService = cursoService;
            _infoEnlaceService = infoEnlaceService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VwplanificacionAcademica c)
        {
            try
            {
                InscribirCursoEVA data = await _cursoService.GuardarCurso(c);
                return Ok(new { datos = data });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> PostInfoEnlace([FromBody] PostInfoEnlace data)
        {
            try
            {
                await _infoEnlaceService.GuardarEquivalencia(data);
                return Ok(new { data.categoryEva, data.categoryUpoli });
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);

            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCategoriasEVA()
        {
            try
            {
                List<CategoryEVA> data = await _infoEnlaceService.ObtenerCategoriasEva();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCategoriasUpoli()
        {
            List<CategoriaUpoliDTO> data = await _infoEnlaceService.ObtenerCategoriasUpoli();
            return Ok(data);
        }

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<VwinfoEquivalente> data = await _infoEnlaceService.ObtenerEquivalencias(paginado);

                return Ok(data);

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);

            }

        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _infoEnlaceService.EliminarEquivalencia(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }

        }
    }
}
