using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moodle.Database;
using moodle.Models.EnlaceEva;
using Microsoft.AspNetCore.Http;
using moodle.Models.bitnami_moodle;
using System.Linq.Dynamic.Core;
using Models;
using Microsoft.AspNetCore.Authorization;
using services;
using System.Threading.Tasks;
using Util;

namespace moodle.Controllers
{
    [Authorize(Policy = "RequireAdministratorRole")]
    [ApiController]
    [Route("api/[controller]")]
    public class SincronizacionController : ControllerBase
    {
        EnlaceEVAContext db;
        MariaDBContext db1;
        private readonly ISyncronizerAlgoritmService _syncronizerAlgoritmService;

        public SincronizacionController(EnlaceEVAContext db, MariaDBContext db1, ISyncronizerAlgoritmService syncronizerAlgoritmService)
        {
            _syncronizerAlgoritmService = syncronizerAlgoritmService;
            this.db = db;
            this.db1 = db1;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DateTime? FechaDesde = null)
        {
            try
            {
                SyncronizacionSaveDTO s = new SyncronizacionSaveDTO()
                {
                    UserId = Request.UserTokenId(),
                    FechaDesde = FechaDesde,
                    TipoSincronizacion = false,
                };

                SyncronizerDTO data = await _syncronizerAlgoritmService.Sincronizar(s);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<SyncronizacionDTO> data = await _syncronizerAlgoritmService.ObtenerSincronizaciones(paginado);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id, [FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<LogError> data = await _syncronizerAlgoritmService.ObtenerErroresSincronizacion(id, paginado);
                return Ok(data);
            }
            catch (Exception er)
            {

                return StatusCode(500, er.Message);
            }
        }
    }
}
