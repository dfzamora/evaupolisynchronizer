using System;
using Microsoft.AspNetCore.Mvc;
using moodle.Database;
using moodle.Models.EnlaceEva;
using services;
using Util;
using Microsoft.AspNetCore.Authorization;
using Models;
using System.Threading.Tasks;

namespace moodle.Controllers
{
    [Authorize(Policy = "RequireAdministratorRole")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsuarioController : ControllerBase
    {
        private EnlaceEVAContext db;
        private readonly IUsuarioService _usuarioService;


        public UsuarioController(EnlaceEVAContext db, IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
            this.db = db;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Usuario usuario)
        {
            try
            {
                await _usuarioService.CrearUsuario(usuario);
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }


        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Usuario usuario)
        {
            try
            {
                await _usuarioService.ActualizarUsuario(usuario);
                return Ok();

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);

            }
        }

        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] Usuario usuario)
        {

            try
            {
                await _usuarioService.CambiarContrasena(usuario);
                return Ok();

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);

            }
        }


        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _usuarioService.EliminarUsuario(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }

        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Paginado paginado)
        {
            try
            {
                Paginacion<Usuario> data = await _usuarioService.ObtenerUsuarios(paginado);

                return Ok(data);

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }

    }


}
