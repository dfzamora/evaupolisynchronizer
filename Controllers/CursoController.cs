using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using moodle.Models.EnlaceEva;
using moodle.Models.bitnami_moodle;
using services;
using System.Threading.Tasks;
using Util;
using Microsoft.AspNetCore.Authorization;

namespace moodle.Controllers
{
    [Authorize(Policy = "RequireAdministratorRole")]
    [ApiController]
    [Route("api/[controller]")]
    public class CursoController : ControllerBase
    {
        private readonly ICursoService _cursoService;

        public CursoController(ICursoService cursoService)
        {
            this._cursoService = cursoService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VwplanificacionAcademica c)
        {
            try
            {
                InscribirCursoEVA data = await _cursoService.GuardarCurso(c);
                return Ok(new { datos = data });

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                List<PlanificacionAcademicaDTO> data = await _cursoService.ObtenerPlanificacionAcademica();
                return Ok(data);

            }
            catch (Exception ex)
            {
                return HandleException.GetExceptionResult(ex);
            }

        }
    }
}
