
using System;

namespace Util
{
    [Serializable]
    public class TokenNotReadableException : Exception
    {
        public TokenNotReadableException() : base("El token de autenticidad no pudo ser leido, verifique la cabecera de la peticíon")
        {
        }

        public TokenNotReadableException(string message)
            : base(message) { }

        public TokenNotReadableException(string message, Exception inner)
            : base(message, inner) { }
    }
}