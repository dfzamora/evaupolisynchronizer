
using System;

namespace Util
{
    [Serializable]
    public class SincronizerServerErrorException : Exception
    {
        public SincronizerServerErrorException() : base("Ocurrió un error de servidor, consulte el Log de errores para obtener mayores detalles")
        {
        }

        public SincronizerServerErrorException(string message)
            : base(message) { }

        public SincronizerServerErrorException(string message, Exception inner)
            : base(message, inner) { }
    }
}