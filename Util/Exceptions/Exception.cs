﻿using System;

namespace Util
{
    public static class ExceptionExtensions
    {
        public static string GetMessage(this Exception e, string msgs = "")
        {
            if (e == null) return string.Empty;
            if (msgs == "") msgs = e.Message;
            if (e.InnerException != null)
                msgs += "\r\nMensaje Interno: " + GetMessage(e.InnerException);
            return msgs;
        }


    }
}