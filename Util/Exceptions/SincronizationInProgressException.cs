
using moodle.Models.EnlaceEva;
using System;

namespace Util
{
    [Serializable]
    public class SincronizationInProgressException : Exception
    {
        public SincronizationInProgressException(Sincronizacion sincro) : base($"La sincronizaci�n {(sincro.Tipo? "Autom�tica" : "Manual")} No.{sincro.IdSincronizacion} a�n se encuentra en proceso, por favor espere a que finalice para iniciar un nuevo proceso")
        {
        }

        public SincronizationInProgressException(string message)
            : base(message) { }

        public SincronizationInProgressException(string message, Exception inner)
            : base(message, inner) { }
    }
}