
using System;

namespace Util
{
    [Serializable]
    public class DuplicatedException : Exception
    {
        public DuplicatedException() : base("El registro ya se encuentra almacenado, intente con valores distintos")
        {
        }

        public DuplicatedException(string message)
            : base(message) { }

        public DuplicatedException(string message, Exception inner)
            : base(message, inner) { }
    }
}