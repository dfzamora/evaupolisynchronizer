
using System;

namespace Util
{
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException() : base("La información buscada no ha sido encontrada, información inválida")
        {
        }
    
        public NotFoundException(string message)
            : base(message) { }

        public NotFoundException(string message, Exception inner)
            : base(message, inner) { }
    }
}