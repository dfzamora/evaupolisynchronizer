using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Util
{
    public static class HandleException
    {
        public static ObjectResult GetExceptionResult(Exception ex)
        {
            ObjectResult objRes = new ObjectResult(ex.Message);
            objRes.StatusCode = StatusCodes.Status500InternalServerError;

            if (ex is HttpStatusException httpStatusEx)
            {
                objRes.StatusCode = (int)httpStatusEx.StatusCode;
                objRes.Value = httpStatusEx.Message;
            }
            else if (ex is NotImplementedException)
            {
                objRes.StatusCode = StatusCodes.Status501NotImplemented;
            }
            else if (ex is DbUpdateException)
            {
                objRes.Value = $"Ocurrió un error, intente mas tarde";
            }
            else if (ex is DuplicatedException)
            {
                objRes.StatusCode = StatusCodes.Status409Conflict;
            }
            else if (ex is SincronizerServerErrorException)
            {
                objRes.StatusCode = StatusCodes.Status500InternalServerError;
            }
            else if (ex is SincronizationInProgressException)
            {
                objRes.StatusCode = StatusCodes.Status412PreconditionFailed;
            }
            else if (ex is TokenNotReadableException)
            {
                objRes.StatusCode = StatusCodes.Status401Unauthorized;
            }

            objRes.Value = new { Mensaje = objRes.Value };

            return objRes;
        }
    }
}