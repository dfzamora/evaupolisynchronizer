using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Util
{

    public class Paginacion<T>
    {

        public List<T> items { get; set; }
        public int total_count { get; set; }
    }
    public class PaginarExtension
    {

        public async Task<Paginacion<T>> paginar<T>(IQueryable<T> obj, Paginado paginado)
        {
            Paginacion<T> p = new Paginacion<T>();
            p.total_count = obj.Count();
            p.items = await obj.OrderBy(paginado.OrdenarPor + " " + paginado.Orden)
                            .Skip(paginado.skip)
                            .Take(paginado.registroPorPagina)
                            .ToListAsync();
            return p;
        }
    }
}