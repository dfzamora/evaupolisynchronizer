using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Util
{
    public static class TokenHeader
    {

        public static int UserTokenId(this HttpRequest request)
        {
            var handler = new JwtSecurityTokenHandler();

            var token = ObtenerTokenHeader(request);
            var jsonToken = handler.ReadJwtToken(token);

            return int.Parse(jsonToken.Payload["nameid"].ToString());
        }

        public static string ObtenerTokenHeader(this HttpRequest request)
        {
            if (request.Headers.TryGetValue("Authorization", out StringValues authToken))
            {
                string[] tokenString = authToken.FirstOrDefault()?.Split("Bearer ");
                if (tokenString.Length >= 2)
                    return tokenString[1];
                else
                    throw new TokenNotReadableException();

            }
            else
                throw new TokenNotReadableException();
        }

    }
}




