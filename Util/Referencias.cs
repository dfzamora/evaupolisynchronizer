namespace Util{
    public partial class  Referencias
    {
        public static string ErrorCurso = "Curso";
        public static string ErrorEstudiante = "Estudiante";
        public static string ErrorCorreoDocente = "Correo Docente";
        public static string ERRORSERVIDOR = "ERROR DE SERVIDOR";
        public static string ErrorCorreoInvalido = "Correo Invalido";
        public static string ErrorDBMoodle = "Base de datos Moodle";
        public static string ErrorCursoDBMoodle = "Curso Base de datos Moodle";
        public static string ErrorEstudianteDBMoodle = "Estudiante Base de datos Moodle";
        public static string ErrorInesperado = "Error no determinado con la Aplicación";
        public static string upoliMailDomain = "upoli.edu.ni";
        public static string ErrorDuplicateEntry = "Duplicate entry";
    }
}