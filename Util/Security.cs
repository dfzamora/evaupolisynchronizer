using System;
using System.Security.Cryptography;
using System.Text;

namespace Util
{
    public class Security
    {

        public static string encryptPass(string pass)
        {
            var data = Encoding.ASCII.GetBytes(pass);
            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(data);
            var hashedPassword = Convert.ToBase64String(md5data);
            return hashedPassword;
        }

    }
}